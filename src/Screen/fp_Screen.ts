//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//-----------------------------------------------------------------
// This file is dirty - it uses window
//-----------------------------------------------------------------

export const getScreenWidth =
  (): number => {
  var screenWidth = 320;
  if (window.matchMedia("(min-width: 1360px)").matches) {
    screenWidth = 1360;
  } else
  if (window.matchMedia("(min-width: 1000px)").matches) {
    screenWidth = 1000;
  } else
  if (window.matchMedia("(min-width: 768px)").matches) {
    screenWidth = 768;
  } else
  if (window.matchMedia("(min-width: 640px)").matches) {
    screenWidth = 640;
  } else
  if (window.matchMedia("(min-width: 480px)").matches) {
    screenWidth = 480;
  } 

  return screenWidth;
}

export const isScreenWidth768 = 
(): boolean => 
  getScreenWidth () >= 1000

