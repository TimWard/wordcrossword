//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { EventObject, MachineConfig } from 'xstate'

// My machine types
import { TMachineIds } from '../crosswordMachineIds';
import { IPlayContext } from './playMachineTypes_Context';


// Imports from the game model
import * as playMachineActions from './playMachineTypes_Actions';
import * as playMachineStates from './playMachineTypes_States';
import * as playMachineEvents from './playMachineTypes_Events';
import * as playMachineGuards from './playMachineTypes_Guards';


type TplayMachineConfig = MachineConfig <IPlayContext, any, EventObject>


export const playMachineConfig: TplayMachineConfig = {
  id: TMachineIds.playMachine,
  type: 'parallel',
  states: {
    [playMachineStates.TTitleAnimationStates.playgame_title_animation]: {
      initial: playMachineStates.TTitleAnimationStates.playgame_title_animation_start,
      states: {
        [playMachineStates.TTitleAnimationStates.playgame_title_animation_start]: {
          entry: [playMachineActions.TTitleLettersActions.assignTitleAnimationCss1],
          after: {
            2000: playMachineStates.TTitleAnimationStates.playgame_title_animation_done
          },
          exit: [playMachineActions.TTitleLettersActions.assignTitleAnimationCss2,
            playMachineActions.TTitleLettersActions.assignTitleLetters],
        },
        [playMachineStates.TTitleAnimationStates.playgame_title_animation_done]: {
          type: 'final'
        },
      },
    },
    [playMachineStates.TPlayMachineStates.play_wrapper]: {
      initial: playMachineStates.TPlayMachineStates.crossword_states,
      states: {
        [playMachineStates.TPlayMachineStates.crossword_states]: {
          initial: playMachineStates.TPlayMachineStates.crossword_editing,
          states: {
            [playMachineStates.TPlayMachineStates.crossword_editing]: {
              onDone: playMachineStates.TPlayMachineStates.crossword_complete, 
              on: {
                [playMachineEvents.TPlayEvents.REVEAL_ALL]: {
                  target: playMachineStates.TPlayMachineStates.reveal_all_confirm,
                  internal: true,
                },                   
              },
              initial: playMachineStates.TPlayMachineStates.clue_editing,
              states: {
                [playMachineStates.TPlayMachineStates.clue_editing]: {
                  onDone: [
                    {
                      target: playMachineStates.TPlayMachineStates.clue_final, 
                      cond: playMachineGuards.TPlayCrosswordGuards.isCrosswordComplete
                    },
                    {
                      target: playMachineStates.TPlayMachineStates.clue_editing, 
                    },
                  ],
                  on: {
                    [playMachineEvents.TPlayEvents.REVEAL_THIS]: {
                      actions: [playMachineActions.TPlayActions.revealThis,   
                                playMachineActions.TPlayActions.assignFromActivePuzzleStates,
                                playMachineActions.TPlayActions.moveToNextClue,
                                playMachineActions.TPlayActions.assignFromActivePuzzleStates],
                                internal: true,
                    },
                    [playMachineEvents.TPlayEvents.SKIP_THIS]: {
                      actions: [playMachineActions.TPlayActions.skipThis,   
                                playMachineActions.TPlayActions.assignFromActivePuzzleStates],
                      internal: true,
                    },
                  },
                  initial: playMachineStates.TPlayMachineStates.square_editing,
                  states: {
                    [playMachineStates.TPlayMachineStates.square_editing]: {
                      on: {
                        [playMachineEvents.TPlayEvents.ENTER_LETTER]: {
                          actions: [ 
                            playMachineActions.TPlayActions.enterLetter,
                            playMachineActions.TPlayActions.assignFromActivePuzzleStates,
                            playMachineActions.TPlayActions.squareUpdate,
                          ],
                        },
                        [playMachineEvents.TPlayEvents.SQUARE_UPDATE]: [
                          // Note that the order of conditional statements is important
                          // as when one is satisfied, then the rest are ignored
                          {
                            cond: playMachineGuards.TPlayClueGuards.isClueComplete,
                            actions: [
                              playMachineActions.TPlayActions.addPoints,   
                              playMachineActions.TPlayActions.moveToNextClue,
                              playMachineActions.TPlayActions.assignFromActivePuzzleStates,
                            ],
                            target: playMachineStates.TPlayMachineStates.square_final
                          },
                          {
                            cond: playMachineGuards.TPlaySquareGuards.isSquareCorrect,
                            actions: [
                              playMachineActions.TPlayActions.moveToNextSquare,
                              playMachineActions.TPlayActions.assignFromActivePuzzleStates,
                            ],
                          },
                        ]
                      },
                    },
                    [playMachineStates.TPlayMachineStates.square_final]: {
                      type: 'final'
                    },
                  },
                },
                [playMachineStates.TPlayMachineStates.clue_final]: {
                  type: 'final'
                },
              },
            },
            [playMachineStates.TPlayMachineStates.crossword_complete]: {
              type: 'final'
            },                            
            [playMachineStates.TPlayMachineStates.reveal_all_confirm]: {
              after: {
                2000: playMachineStates.TPlayMachineStates.crossword_editing
              },
              on: {
                [playMachineEvents.TPlayEvents.REVEAL_ALL_CONFIRM]: {
                  target: playMachineStates.TPlayMachineStates.crossword_editing,
                  actions: [playMachineActions.TPlayActions.revealAll,   
                            playMachineActions.TPlayActions.assignFromActivePuzzleStates],
                  internal: true,
                },
              },
            },
          },
          on: {
            [playMachineEvents.TPlayEvents.ENTER_CHAR]: {
              actions: playMachineActions.TPlayActions.squareKeyDown,   
              internal: true,
            },
            [playMachineEvents.TPlayEvents.ENTER_ARROW]: {
              actions: [ 
                playMachineActions.TPlayActions.enterArrow,
                playMachineActions.TPlayActions.assignFromActivePuzzleStates,
              ],
            },
            [playMachineEvents.TPlayEvents.ENTER_TAB]: {
              actions: [ 
                playMachineActions.TPlayActions.enterTab,
                playMachineActions.TPlayActions.assignFromActivePuzzleStates,
              ],
            },
            [playMachineEvents.TPlayEvents.SELECT_CLUE]: {
              actions: [playMachineActions.TPlayActions.assignActiveClue,   
                        playMachineActions.TPlayActions.assignFromActivePuzzleStates],
              internal: true,
            },
          },
          onDone: playMachineStates.TPlayMachineStates.game_over
        },
        [playMachineStates.TPlayMachineStates.game_over]: {
          type: 'final'
        },
      },
    },
  }
}

