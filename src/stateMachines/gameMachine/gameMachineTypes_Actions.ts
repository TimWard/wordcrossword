//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------


export const TClueWordsActions = {
  assignCluewords:        'assignCluewords',
} 

export const TGridActions = {
  assignGrids:        'assignGrids',
} 

export const TGameLanguageSelectActions = {
  assignActiveClueLang:         'assignActiveClueLang',
} 


