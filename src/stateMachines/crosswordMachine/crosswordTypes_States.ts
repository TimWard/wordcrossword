//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//-------------------------------------------------------------------------
// Crossword
//-------------------------------------------------------------------------

export const TCrosswordStates = {
  loadCategories:   'loadCategories',
  loadLanguages:    'loadLanguages',
  loadWebsitetext:  'loadWebsitetext',
  selectPage:       'selectPage',
  selectOptions:    'selectOptions',
  selectSiteLang:   'selectSiteLang',
  selectClueLang:   'selectClueLang',
  selectCrossLang:  'selectCrossLang',
} 


export const TLanguageStates = {
  loading:            'languages_loading',
  crosslang_loader:   'languages_crosslang_loader',
  cluelang_loader:    'languages_cluelang_loader',
  loaded:             'languages_loaded',
  loading_error:      'languages_loading_error',
} 



export const TPageStates = {
  crosswords:   'crosswords',
  dictionary:   'dictionary',
  feedback:     'feedback',
} 

export const TCategoryStates = {
  categories_loading:       'categories_loading',
  categories_loaded:        'categories_loaded',
  categories_loading_error: 'categories_loading_error',
  selectCategory:           'selectCategory',
  loadPuzzles:              'loadPuzzles',
} 

export const TPuzzleStates = {
  puzzles_loading:        'puzzles_loading',
  puzzles_loaded:         'puzzles_loaded',
  puzzles_loading_error:  'puzzles_loading_error',
} 


export const TWebsitetextStates = {
  websitetext_loading:       'websitetext_loading',
  websitetext_loaded:        'websitetext_loaded',
  websitetext_loading_error: 'websitetext_loading_error',
} 



