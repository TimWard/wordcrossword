//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
import { IWebsitetextElement, IWebsitetextProps, IWebsitetextExtraProps } from './WebsitetextTypes';


export const WebsitetextElement: IWebsitetextElement = (props: IWebsitetextProps) =>
  <div> {props.getTranslatedText (props.text)} </div>


export const WebsitetextExtra = (props: IWebsitetextExtraProps) =>
  <div> {props.getTranslatedText (props.text) + props.additionalText} </div>

