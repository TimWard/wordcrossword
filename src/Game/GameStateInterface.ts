//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// Interfaces
import { IPuzzle } from 'crosstypes';
import {ISquare} from 'crosstypes';
import {IClue} from 'crosstypes';

// State Machine types
import { TCrosswordState } from '../stateMachines/crosswordMachine/crosswordTypes';
import { TCrosswordEventTypes } from '../stateMachines/crosswordMachine/crosswordMachine_Events';


type IMyEventType = 
string 
| TCrosswordEventTypes

// This definition is dodgy!
export type ISendCrosswordMachineState = (event: IMyEventType, payload?: any) => TCrosswordState






