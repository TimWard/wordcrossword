//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import * as O from 'fp-ts/lib/Option'

import { IGameData } from 'dblib';

import { IClue, ISquare, IPuzzle, ILanguage, ICategory, IWord } from 'crosstypes';

export interface IPuzzleStates {
  activePuzzle: IPuzzle
  activeClue:   IClue,
  activeSquare: ISquare,
}

export interface IPlayContext {
  // Passed in and read-only
  activeClueLang: ILanguage
  activeCrossLang: ILanguage
  activeCategory: ICategory
  gameData: IGameData
  clueWords: O.Option <IWord[]>

  tempPuzzleStates: IPuzzleStates

  activePuzzle: IPuzzle // Passed in and read-write
  activeClue:   IClue
  activeSquare: ISquare

  titleLetters: string
  titleAnimationCss: string

  points: number
}
