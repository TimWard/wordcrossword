//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------
// This file is pure - no DB, no IO, no dirty imports
//------------------------------------------------

import { IGrid, createEmptyGrid } from "crosstypes";



export const shallowCopyGrid = 
(grid: IGrid)
: IGrid => {
  return { 
    // DB fields
    id: grid.id,
    boardsize: grid.boardsize,
    // Computed fields
    blankwords: [], 
    squares: [], 
   }
}



  