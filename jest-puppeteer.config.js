module.exports = {
  // server: {
  //   command: `npm start`,
  //   port: 3000,
  //   launchTimeout: 20000
  // },
  launch: {
    // Works with chromium, chrome and brave
//    executablePath: 'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe',
    executablePath: 'C:\\Program Files (x86)\\BraveSoftware\\Brave-Browser\\Application\\brave.exe',
    args:['--start-maximized'],
//    headless: true,
    headless: false,
//    defaultViewport: {width: 1360, height: 768}, // Without this, the window is set to 800 x 600
    defaultViewport: null, // Without this, the window is set to 800 x 600
    slowMo: 100
  }
};
