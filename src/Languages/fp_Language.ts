//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------
// This file is pure - no DB, no IO, no dirty imports
//------------------------------------------------

import { ILanguage } from "crosstypes";


export const createLanguage =
 (id: number, name: string, alphabetId: number, code: string, flag: string)
 : ILanguage => {
  return {
  id:          id,
  name:        capitalizeFirstLetter (name),
  alphabetid:  alphabetId,
  code:        code,
  flag:        flag,
  }
}

  // This is used in testing when we only have the english words (translated into english)
  // and we wan't to create crosswords in english
export const createDefaultLanguage =
(): ILanguage =>
  createLanguage (4,'English Test', 1, 'EN', 'GB')
  
export const createDefaultCrossLanguage =
(): ILanguage =>
  createLanguage (32,'Spanish Test', 1, 'ES', 'ES')
  
export const createErrorCrossLanguage =
(error: string): ILanguage =>
  createLanguage (-1,'Error loading', 1, 'ES', 'ES')
  
    
export const borderColor =
(activeLanguage: ILanguage) =>
(otherLangugage: ILanguage)
: boolean =>
  activeLanguage.name === otherLangugage.name

// To do - This should be in a string utils file
export const capitalizeFirstLetter =
(text: string):
string =>
  text.charAt(0).toUpperCase() + text.slice(1)
    
