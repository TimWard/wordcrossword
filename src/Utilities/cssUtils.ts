//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';
import * as A from 'fp-ts/lib/Array'

export const getRandomColor = (): string => 
  pipe (
    A.makeBy (6, () => Math.floor(Math.random() * 16)),
    A.map (n => n.toString (16)),
    A.reduce ('#', (a,b) => a.concat(b))
  )


export const cssJoin = (arr: string[]): string =>
  arr.join (' ')
