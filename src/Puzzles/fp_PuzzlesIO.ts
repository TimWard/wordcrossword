//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------
// This file is now pure - using tagless final
//------------------------------------------------

// Other 3rd party imports
import produce from 'immer';

// fp-ts
import * as T from 'fp-ts/lib/Task'
import { pipe } from "fp-ts/lib/pipeable";
import * as A from 'fp-ts/lib/Array'

// Interfaces
import { IPuzzle } from "crosstypes";
import { IGameData } from "dblib";

import * as fp_Clues from '../Clues/fp_Clues';

// Utilities
//import * as Logging from "../Utilities/Logging";


const loadPuzzlesFromDB =
(gameData: IGameData) =>
(languageId: number, categoryId: number, dayIndex: number)
: T.Task <IPuzzle[]> => 
  gameData.dailyPuzzleData.loadTodaysDailyPuzzleFromDb (languageId, categoryId)


// Todo1 - move the loading of clues to the clues component!
// Note that the clues must load BEFORE the grid loads
// That is the clues, not the clue component which just loads the clues translated 
export const loadWordsForPuzzleFromDB =
(gameData: IGameData) =>
(puzzle: IPuzzle)
: T.Task <IPuzzle> => 
  pipe (
    gameData.wordData.loadWordsByPuzzleFromDB (puzzle.languageid) (puzzle.id),
    T.map ( crossWords => produce (puzzle, draft => {draft.crossWords = crossWords}) ),
  )  

export const loadCluesForPuzzleFromDB =
(gameData: IGameData) =>
(languageCode: string) => 
(activeCrossLangAlphabetId: number) => 
(puzzle: IPuzzle)
: T.Task <IPuzzle> => 

  pipe (
    gameData.clueData.loadAndPopulateClues (puzzle.crossWords) (puzzle.id),
    T.map (A.map ( fp_Clues.createRandomPaddedTitle (activeCrossLangAlphabetId) (languageCode))),
    T.map ( clues => produce (puzzle, draft => {draft.clues = clues}) ),
  )  
  

// README - keeping this here to show how to convert messy code
// to clean code using A.traverse
// export const loadPuzzlesForLangAndCategory_old = 
// (gameData: IGameData) =>
// (languageId: number) => 
// (languageCode: string) => 
// (categoryId: number ) =>
// (dayIndex: number)
// : T.Task <IPuzzle[]> => 
//   pipe (
//     loadPuzzlesFromDB (gameData) (languageId, categoryId, dayIndex),
//     T.chain ( puzzles => 
//       pipe (
//         puzzles, 
//         A.map ( puzzle => 
//           pipe (
//             loadWordsForPuzzleFromDB (gameData) (puzzle), 
//             T.chain (loadCluesForPuzzleFromDB (gameData) (languageCode)),
//           ) 
//         ), 
//         A.array.sequence (T.taskSeq) 
//       )
//     )
//   )

// Note that although this works on an array of puzzles, 
// there should only be one puzzle per category / language / day
export const loadPuzzlesForLangAndCategory = 
(gameData: IGameData) =>
(languageId: number) => 
(languageCode: string) => 
(activeCrossLangAlphabetId: number) => 
(categoryId: number ) =>
(dayIndex: number)
: T.Task <IPuzzle[]> => 

  pipe (
    loadPuzzlesFromDB (gameData) (languageId, categoryId, dayIndex),
    T.chain ( A.traverse (T.taskSeq) ( loadWordsForPuzzleFromDB (gameData) )), 
    T.chain ( A.traverse (T.taskSeq) ( loadCluesForPuzzleFromDB (gameData) (languageCode) (activeCrossLangAlphabetId))),
  )



