//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// This requires documenting!
import * as React from 'react';
import {countries} from './countries';

// CSS files
import './css/flags.css';
import './css/flags320.css';
import './css/flags1000.css';


interface IFlagsState {
	disabled: boolean;
	openOptions: boolean;
	defaultCountry: string;
	filteredCountries: string[];
	selected: string;
	filter: '';
	countries: string[];
}


interface IFlagsProps {
	countries: string[];
	defaultCountry: string;
	onSelect: (flag: string) => void
	customLabels:  {key: string, value: string}[]
	blackList: boolean;
	disabled: boolean;
	alignOptions: string;
	className: string;
	showSelectedLabel: boolean;
	showOptionLabel: boolean;
	placeholder: string;
	searchable: boolean;
	searchPlaceholder: string;
}



const getLabelValue = 
  (customLabels:  {key: string, value: string}[]) =>
  (selectedValue: string) => {
    const label = customLabels.find (label => label.key === selectedValue)
    return label ? label.value : ''  
  }

class ReactFlags extends React.Component <IFlagsProps, IFlagsState> {

	public static defaultProps = {
		className: '',
		countries: [],
		placeholder: "Select a country",
		showSelectedLabel: true,
		showOptionLabel: true,
		alignOptions: "right",
		customLabels: [],
		disabled: false,
		blackList: false,
		searchable: false,
		searchPlaceholder: 'Search',
	}
	
	constructor(props: IFlagsProps){
		super(props);

		const defaultCountry = countries[this.props.defaultCountry] && this.props.defaultCountry;

		this.state = {
			disabled: false,
			openOptions: false,
			defaultCountry: defaultCountry,
			filteredCountries: [],
			selected: '',
			filter: '',
			countries: [],
		}

		this.toggleOptions = this.toggleOptions.bind(this);
		this.closeOptions = this.closeOptions.bind(this);
		this.onSelect = this.onSelect.bind(this);
		this.filterSearch = this.filterSearch.bind(this);
		this.setCountries = this.setCountries.bind(this);
	}

	toggleOptions() {
		!this.state.disabled && this.setState({
			openOptions: !this.state.openOptions
		});
	}

	toggleOptionsWithKeyboard(evt: any) {
		evt.preventDefault();
		if (evt.keyCode === 13) {
			//enter key: toggle options
			this.toggleOptions();
		} else if (evt.keyCode === 27) {
			//esc key: hide options
			!this.state.disabled && this.setState({
				openOptions: false
			});
		}

	}

	closeOptions(event: any) {
		if (event.target !== this.refs.selectedFlag && event.target !== this.refs.flagOptions && event.target !== this.refs.filterText ) {
			this.setState({
				openOptions: false
			});
		}
	}

	async onSelect(countryCode: string) {
		this.setState({
			selected: countryCode,
			filter : ''
		})
		await this.props.onSelect(countryCode)
	}

	async onSelectWithKeyboard(evt: any, countryCode: string) {
		evt.preventDefault();
		if (evt.keyCode === 13) {
			//enter key: select
			await this.onSelect (countryCode)
			this.closeOptions(evt);
		} else if (evt.keyCode === 27) {
			//esc key: hide options
			this.toggleOptions();
		}
	}

	updateSelected(countryCode: string) {
		let isValid = countries[countryCode];

		isValid && this.setState({
			selected: countryCode
		})
	}

	filterSearch(evt: any) {
		let filterValue = evt.target.value;
		let filteredCountries = filterValue && this.state.countries.filter(key => {
			let label = getLabelValue (this.props.customLabels) (key)

			return label && label.match(new RegExp(filterValue, 'i'))
		}) 

		this.setState({filter : filterValue, filteredCountries : filteredCountries });
	}

	setCountries() {
		const fullCountries = Object.keys(countries);

		let selectCountries = this.props.countries && this.props.countries.filter( (country: string) => {
			return countries[country] ;
		});

		//Filter BlackList
		if (this.props.blackList && selectCountries) {
			selectCountries = fullCountries.filter(countryKey =>{
					return selectCountries.filter((country: string) =>{
						return countryKey === country;
					}).length === 0
			});
		}

		this.setState({
			countries: selectCountries || fullCountries
		}, ()=> {
			const { selected } = this.state;

			if (selected && !this.state.countries.includes(selected)) {
				this.setState({ selected: '' });
			}
		});
	}

	componentDidMount() {
		this.setCountries();
		!this.props.disabled && window.addEventListener("click", this.closeOptions);
	}

	componentDidUpdate(prevProps: IFlagsProps) {
		if (prevProps.countries !== this.props.countries || prevProps.blackList !== this.props.blackList) {
			this.setCountries();
		}
	}

	componentWillUnmount() {
		!this.props.disabled && window.removeEventListener("click", this.closeOptions);
	}

	render() {

		let isSelected: string = this.state.selected || this.props.defaultCountry || this.state.defaultCountry;
		let alignClass = this.props.alignOptions.toLowerCase() === 'left' ? 'to--left' : '';

		return (
			<div className={`flag-select ${this.props.className ? this.props.className :  ""}`}>
				<div ref="selectedFlag" 
						className={`selected--flag--option ${this.props.disabled ? 'no--focus' : ''}`} 
						tabIndex = {0} 
						onClick={this.toggleOptions} 
						onKeyUp={evt => this.toggleOptionsWithKeyboard(evt)}>
					{isSelected &&
						<span 	className="country-flag" 
            >
							<img 	src={require(`./flags/${isSelected.toLowerCase()}.svg`)} alt={isSelected}/>
							{this.props.showSelectedLabel &&
								<span className="country-label">
									{ getLabelValue (this.props.customLabels) (isSelected) }
								</span>
							}
						</span>
					}

					{!isSelected &&
						<span className="country-label">
							{this.props.placeholder}
						</span>
					}
					<span className={`arrow-down ${this.props.disabled ? 'hidden' : ''}`}>
						▾
					</span>
				</div>

				{this.state.openOptions &&
					<div 	ref="flagOptions" className={`flag-options ${alignClass}`}>
						{this.props.searchable &&
							<div className="filterBox">
								<input 	type="text" 
										placeholder={this.props.searchPlaceholder} 
										ref="filterText"  
										onChange={this.filterSearch}/>
							</div>
						}
						{(this.state.filter ? this.state.filteredCountries : this.state.countries).map( countryCode =>

							<div 	className={`flag-option ${this.props.showOptionLabel ? 'has-label' : ''}`} 
									key={countryCode} 
									tabIndex = {0} 
									onClick={async () => await this.onSelect(countryCode)} 
									onKeyUp={async evt => await this.onSelectWithKeyboard(evt, countryCode)} >
                <span className="country-flag country-flag-options" >
									<img	src={require(`./flags/${countryCode.toLowerCase()}.svg`)} alt={'Flag of Country'} />
									{this.props.showOptionLabel &&
										<span className="country-label">
                      { getLabelValue (this.props.customLabels) (countryCode) }
										</span>
									}
								</span>
							</div>
						)}
					</div>
				}
			</div>
		)
	}
}


export {ReactFlags};
