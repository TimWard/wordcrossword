//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------
// This file is pure - no tasks, no IO
//------------------------------------------------

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';
import * as A from 'fp-ts/lib/Array'
import * as M from 'fp-ts/lib/Monoid'

// pure imports
import { ISquare, ISquarePredicate, Square } from "crosstypes";


export const executePredicateForAllSquares = 
(squares: ISquare[][]) =>
(predicate: ISquarePredicate)
: boolean => 
  pipe (
    A.flatten (squares),
    A.foldMap (M.monoidAll) (predicate)
  )



  
  

 


export const isCrosswordCorrect = 
(squares: ISquare[][])
: boolean =>
  executePredicateForAllSquares 
    (squares) (Square.isSquareCorrect) 

export const isCrosswordClean = 
(squares: ISquare[][])
: boolean =>
  executePredicateForAllSquares 
    (squares) (Square.isSquareClean) 

  

