//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

/*eslint @typescript-eslint/no-unused-expressions: 0*/


//------------------------------------------------------
// This file is pure - no tasks, no IO, no dirty imports
//------------------------------------------------------

// Other 3rd party imports
import produce, {Draft} from 'immer';

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';
import * as A from 'fp-ts/lib/Array'

// interfaces
import { ISquare, ISquareCallback, IWebsitetext, IPuzzle, IClue, ILanguage } from "crosstypes";

// Pure fp imports
import * as fp_Squares from '../Squares/fp_Squares';
import {cf_Blankword} from 'crossfuncs';
import * as fp_GameTitleLetter from '../GameTitle/fp_GameTitleLetter';
import * as fp_Clue from '../Clues/fp_Clue';
import * as fp_Clues from '../Clues/fp_Clues';
import * as fp_Websitetexts from '../Websitetext/fp_Websitetexts'


// Todo - this import is dirty!!!
import * as fp_Utils from './fp_Utils';
import { IPlayContext, IPuzzleStates } from '../stateMachines/playMachine/playMachineTypes_Context';


// Utility Functions for squares 
//------------------------------
export const executeForAllSquaresInClue = 
(machineState: IPuzzleStates) =>
(mutate_callback: ISquareCallback)
: IPuzzleStates => 

  // Precondition based on the state
  !cf_Blankword.isCorrect (machineState.activePuzzle.grid.squares) (machineState.activeClue.blankword)
  ? // Execute the side effects 
  pipe (
    produce (machineState, draft => 
      cf_Blankword.mutate_executeSideEffectForAllSquaresInBlankword 
       ( draft.activePuzzle.grid.squares) 
        (draft.activeClue.blankword) 
        (mutate_callback) 
    ),
   newStates => getActiveClueAndActiveSquare (newStates) (newStates.activeClue)
  )
  : machineState
  
export const executeForAllSquares= 
(machineState: IPuzzleStates) =>
(mutate_callback: ISquareCallback)
: IPuzzleStates => 

  // Execute the side effects 
  pipe (
    produce (machineState, draft => {
      pipe (
        A.flatten (draft.activePuzzle.grid.squares),
        A.map (mutate_callback)
      )}
    ),
    newStates => getActiveClueAndActiveSquare (newStates) (newStates.activeClue)
  )
  
    
export const doExecuteForAllSquaresInClue =
(machineState: IPuzzleStates) =>
(callback: ISquareCallback)
: IPuzzleStates => 
  executeForAllSquaresInClue 
    (machineState) 
    (callback)


  


  
// The page title - WORDCROSSWORD
//----------------------------------------
export const getAlphabet = 
(language: ILanguage)
: string[] =>
  pipe (
    language.alphabetid,
    fp_GameTitleLetter.getAlphabet
  )


//---------------------------------------------------------
// Functions that handle the pure side of processing events
//---------------------------------------------------------


// Wrapper functions to implement ISquareProps 
export const isCrosswordCorrect = 
(activePuzzle: IPuzzle)
: boolean => 
  fp_Squares.isCrosswordCorrect (activePuzzle.grid.squares)    

export const handleSquareClick = 
(machineState: IPuzzleStates) => 
(square: ISquare)
: IPuzzleStates => 
  produce (machineState, draft => {
    draft.activeClue    = fp_Clue.getClueFromSquare (machineState.activeClue) (machineState.activeSquare) (square)
    draft.activeSquare  = square
  })
  


export const handleTabKeyPress = 
(machineState: IPuzzleStates) => 
(forward: boolean = true)
: IPuzzleStates =>
  pipe (
    fp_Clues.getNextIncorrectClueByDirection 
      (forward) (machineState.activePuzzle.grid.squares) (machineState.activePuzzle.clues) (machineState.activeClue),
      getActiveClueAndActiveSquare (machineState)
  )



export const handleSquareKeyPress = 
(context: IPlayContext) => 
(event: KeyboardEvent)
: IPuzzleStates => 
  pipe (
    context.activeCrossLang.alphabetid,
    fp_Utils.isAlphaChar (event.key)
  )
    ? handleSquareCharKeyPress (context) (event.key)
    : fp_Utils.isNumericChar (event.key)
      ? handleSquareNumericKeyPress (context) (parseInt(event.key))
      : context

export const newHandleSquareKeyPress = 
(machineState: IPlayContext) =>
(key: string)
: IPuzzleStates => 
  pipe (
    machineState.activeCrossLang.alphabetid,
    fp_Utils.isAlphaChar (key)
  )
    ? handleSquareCharKeyPress (machineState) (key)
    : fp_Utils.isNumericChar (key)
      ? handleSquareNumericKeyPress (machineState) (parseInt(key))
      : machineState

export const handleSquareNumericKeyPress = 
(machineState: IPuzzleStates) => 
(clueNo: number)
: IPuzzleStates =>
  pipe (
    machineState.activeClue,
    fp_Clues.findClueByClueNo (machineState.activePuzzle.grid.squares) (machineState.activePuzzle.clues) (clueNo),
    getActiveClueAndActiveSquare (machineState) 
  )

export const handleSquareCharKeyPress = 
(machineState: IPuzzleStates) => 
(letter: string)
: IPuzzleStates => 

    produce (machineState, draft => mutate_updateLetter (draft) (letter) )
  
// Warning - when using immer, we have to make sure that duplicate state is kept in sync
// i.e. when we mutate a square, then a new object is created,
// if the active square points to this square, then we have to update it to point at the new object!
const mutate_updateLetter =
(draft: Draft <IPuzzleStates>) => 
(letter: string)
: void => { 
  draft.activePuzzle.grid.squares[draft.activeSquare.row][draft.activeSquare.col].userLetter = letter.toLowerCase()
  draft.activeSquare = draft.activePuzzle.grid.squares[draft.activeSquare.row][draft.activeSquare.col]
}


//--------------------------------------------------------
// Clues section
//--------------------------------------------------------


export const findActiveClue = 
(squares: ISquare[][]) =>
(activePuzzle: IPuzzle) =>
(clue: IClue)
: IClue => 
  cf_Blankword.isCorrect (squares) (clue.blankword)
    ? isCrosswordCorrect (activePuzzle)
      ? activePuzzle.clues[0]
      // Get the next incorrect clue 
      : fp_Clues.getNextIncorrectClue (squares) (activePuzzle.clues) (clue)
    : clue

  
export const getActiveClueAndActiveSquare = 
(machineState: IPuzzleStates) =>
(clue: IClue)
: IPuzzleStates => {
  let activeClue   = findActiveClue (machineState.activePuzzle.grid.squares) (machineState.activePuzzle) (clue)
  let activeSquare = getActiveSquareFromActiveClue (machineState.activePuzzle.grid.squares) (activeClue) 

  return produce (machineState, draft => {
     draft.activeClue   = activeClue
     draft.activeSquare = activeSquare
  })
}

export const getNextActiveSquare = 
(machineState: IPuzzleStates) =>
(clue: IClue)
: IPuzzleStates => {
  let activeSquare = getActiveSquareFromActiveClue (machineState.activePuzzle.grid.squares) (clue) 

  return produce (machineState, draft => {
     draft.activeSquare = activeSquare
  })
}


export const doSelectClue = 
(machineState: IPuzzleStates) =>
(clue: IClue)
: IPuzzleStates => {
  let activeSquare = getActiveSquareFromActiveClue (machineState.activePuzzle.grid.squares) (clue) 

  return produce (machineState, draft => {
     draft.activeClue   = clue
     draft.activeSquare = activeSquare
  })
}

export const doSelectSquare = 
(machineState: IPuzzleStates) =>
(square: ISquare)
: IPuzzleStates => {
  let activeClue   = fp_Clue.getClueFromSquare (machineState.activeClue) (machineState.activeSquare) (square)
  let activeSquare = getActiveSquareFromActiveClue (machineState.activePuzzle.grid.squares) (activeClue) 

  return produce (machineState, draft => {
    draft.activeSquare = activeSquare
    draft.activeClue   = activeClue
  })
}
  
  
export const getNextIncorrectClueAndSquare =
(machineState: IPuzzleStates)
:IPuzzleStates => 
  pipe (
    fp_Clues.getNextIncorrectClue (machineState.activePuzzle.grid.squares) (machineState.activePuzzle.clues) (machineState.activeClue),
    getActiveClueAndActiveSquare (machineState)  
  )

export const getActiveSquareFromActiveClue =
(squares: ISquare[][]) =>
(activeClue: IClue)
: ISquare => 
  pipe (
    cf_Blankword.blankwordSquares (activeClue.blankword) (squares),
    mySquares => fp_Clue.getFirstIncorrectSquareAfterActiveSquare (squares)
       (mySquares[0]) (activeClue)
  )

  
//--------------------------------------------------------
// Category section
//--------------------------------------------------------

export const getTranslatedText = 
(websitetexts: IWebsitetext[]) =>
(englishText: string): string => 
  fp_Websitetexts.getTranslatedText (websitetexts) (englishText)

 
