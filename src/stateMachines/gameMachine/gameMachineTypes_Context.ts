//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import * as O from 'fp-ts/lib/Option'

import { IPuzzle, ILanguage, ICategory, IWord } from 'crosstypes';

import { IGameData } from 'dblib';

export interface IGameContext {
  // Passed in and read-only
  activeClueLang: ILanguage
  activeCrossLang: ILanguage
  activeCategory: ICategory
  clueWords: O.Option <IWord[]>
  // Should we replace IGameData with only the interfaces we actually use?
  gameData: IGameData

  activePuzzle: IPuzzle // Passed in and read-write
}
