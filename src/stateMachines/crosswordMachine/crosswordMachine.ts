//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import * as O from 'fp-ts/lib/Option'

// Import the universal Ids
import { TMachineIds, TInvokedMachineIds } from '../crosswordMachineIds'

// My machine types
import * as crosswordStates from './crosswordTypes_States'
import * as crosswordActions from './crosswordTypes_Actions'
import * as crosswordServices from './crosswordTypes_Services'
import * as crosswordEvents from './crosswordTypes_Events'
import { TCrosswordMachineConfig } from './crosswordTypes'

import * as languageEvents from '../languageTypes_Events';

// Import the sub-machines
import { gameMachine } from '../gameMachine/gameMachine_Options'
import { langLoadingMachine } from '../languagesModel/languagesMachine_Options'
  

export const crosswordMachineConfig: TCrosswordMachineConfig = {
  id: TMachineIds.crosswordMachine,
  initial: crosswordStates.TCrosswordStates.loadLanguages,
  states: {
    [crosswordStates.TCrosswordStates.loadLanguages] : {
      initial: crosswordStates.TLanguageStates.loading,
      states: {
        [crosswordStates.TLanguageStates.loading]: {
          invoke: {
            id: TInvokedMachineIds.loadingMachine,
            src: langLoadingMachine,
            data: {
              // WARNING!!!
              // We have to specify the entire context here
              // because this will trash any context in the sub-machine
              // this will be fixed in xState v5, apparently
              siteLangId: (context: any, event: any) => 4,
              dayIndex: (context: any, event: any) => 1,
              crossLangs: (context: any, event: any) => [],
              clueLangs: (context: any, event: any) => [],
              siteLangs: (context: any, event: any) => [],
              languageData: (context: any, event: any) => context.gameData.languageData,
            },
            onDone: {
              target: crosswordStates.TLanguageStates.loaded,
              actions: crosswordActions.TLanguageMachineActions.assignLanguagesFromMachine,
            },
            onError: {
              target: crosswordStates.TLanguageStates.loading_error,
              actions: crosswordActions.TLanguageMachineActions.assignLanguagesErrorFromMachine,
            }
          }
        },
        [crosswordStates.TLanguageStates.loaded]: {
          type: 'final',
        },
        [crosswordStates.TLanguageStates.loading_error]: {
          type: 'final'
        }
      },
      onDone: crosswordStates.TCrosswordStates.selectOptions
    },
    [crosswordStates.TCrosswordStates.selectOptions] : {
      type: 'parallel',
      states: {
        [crosswordStates.TCrosswordStates.selectPage] : {
          initial: crosswordStates.TPageStates.crosswords,
          states: {
            [crosswordStates.TPageStates.crosswords]: {
              on: {
//                [crosswordEvents.TPageEvents.CLICK_DICTIONARY] : crosswordStates.TPageStates.dictionary
                [crosswordEvents.TPageEvents.CLICK_FEEDBACK] : crosswordStates.TPageStates.feedback
              }
            },
            [crosswordStates.TPageStates.feedback]: {
              on: {
                [crosswordEvents.TPageEvents.CLICK_CROSSWORDS] : crosswordStates.TPageStates.crosswords
              }
            }
            // [crosswordStates.TPageStates.dictionary]: {
            //   on: {
            //     [crosswordEvents.TPageEvents.CLICK_CROSSWORDS] : crosswordStates.TPageStates.crosswords
            //   }
            // }
          }
        },    
        [crosswordStates.TCrosswordStates.selectSiteLang]: {
          initial: crosswordStates.TCrosswordStates.loadWebsitetext,
          states: {
            [crosswordStates.TCrosswordStates.loadWebsitetext]: {
              initial: crosswordStates.TWebsitetextStates.websitetext_loading,
              states: {
                [crosswordStates.TWebsitetextStates.websitetext_loading]: {
                  invoke: {
                    id: 'websitetext_loading_Id',
                    src: crosswordServices.TWebsitetextServices.websitetext_loading_svc,
                    onDone: {
                      target: crosswordStates.TWebsitetextStates.websitetext_loaded,
                      actions: crosswordActions.TWebsitetextActions.assignWebsitetext
                    },
                    onError: {
                      target: crosswordStates.TWebsitetextStates.websitetext_loading_error,
//                      actions: TWebsitetextActions.assignCategoriesError
                    }
                  }
                },
                [crosswordStates.TWebsitetextStates.websitetext_loaded]: {
                  type: 'final'
                },
                [crosswordStates.TWebsitetextStates.websitetext_loading_error]: {
                  type: 'final'
                }
              },
            }
          },
          on: {
            [languageEvents.TLanguageEvents.SELECT_SITELANG]: {
              target:   crosswordStates.TCrosswordStates.selectSiteLang,
              actions:  crosswordActions.TCrosswordLanguageSelectActions.assignActiveSiteLang,
            }
          }
        },
        [crosswordStates.TCrosswordStates.selectClueLang]: {
          on: {
          // This event is also handled in the game machine
          // We handle it here to keep the crossword state in sync with the game
          // Then, if the game is reloaded, e.g. due to selecting a different puzzle,
          // then it is supplied with the user selected clue lang
          [languageEvents.TLanguageEvents.SELECT_CLUELANG]: {
              target:   crosswordStates.TCrosswordStates.selectClueLang,
              actions:  crosswordActions.TCrosswordLanguageSelectActions.assignActiveClueLang,
            }
          }
        },
        [crosswordStates.TCrosswordStates.selectCrossLang]: {
          initial: crosswordStates.TCrosswordStates.loadCategories,
          states: {
            [crosswordStates.TCrosswordStates.loadCategories]: {
              initial: crosswordStates.TCategoryStates.categories_loading,
              states: {
                [crosswordStates.TCategoryStates.categories_loading]: {
                  invoke: {
                    id: 'categories_loading_Id',
                    src: crosswordServices.TCategoriesServices.categories_loading_svc,
                    onDone: {
                      target: crosswordStates.TCategoryStates.categories_loaded,
                      actions: crosswordActions.TCategoriesActions.assignCategories
                    },
                    onError: {
                      target: crosswordStates.TCategoryStates.categories_loading_error,
                      actions: crosswordActions.TCategoriesActions.assignCategoriesError
                    }
                  }
                },
                [crosswordStates.TCategoryStates.categories_loaded]: {
                  type: 'final'
                },
                [crosswordStates.TCategoryStates.categories_loading_error]: {
                  type: 'final'
                }
              },
              onDone: crosswordStates.TCategoryStates.selectCategory
            },
            [crosswordStates.TCategoryStates.selectCategory]: {
              initial: crosswordStates.TCategoryStates.loadPuzzles,
              states: {
                [crosswordStates.TCategoryStates.loadPuzzles]: {
                  initial: crosswordStates.TPuzzleStates.puzzles_loading,
                  states: {
                    [crosswordStates.TPuzzleStates.puzzles_loading]: {
                      invoke: {
                        id: 'puzzles_loading_Id',
                        src: crosswordServices.TPuzzleServices.puzzles_loading_svc,
                        onDone: {
                          target: crosswordStates.TPuzzleStates.puzzles_loaded,
                          actions: crosswordActions.TPuzzleActions.assignPuzzles
                        },
                        onError: {
                          target: crosswordStates.TPuzzleStates.puzzles_loading_error,
    //                      actions: TWebsitetextActions.assignCategoriesError
                        }
                      }
                    },
                    [crosswordStates.TPuzzleStates.puzzles_loaded]: {
                      invoke: {
                        id: TInvokedMachineIds.gameMachine,
                        src: gameMachine,
                        autoForward: true, // Todo - this is not recommended!
                        data: {
                          // WARNING!!!
                          // We have to specify the entire context here
                          // because this will trash any context in the sub-machine
                          // this will be fixed in xState v5, apparently
                          activePuzzle: (context: any, event: any) => context.activePuzzle,
                          activeClueLang: (context: any, event: any) => context.activeClueLang,
                          activeCrossLang: (context: any, event: any) => context.activeCrossLang,
                          activeCategory: (context: any, event: any) => context.activeCategory,
                          gameData: (context: any, event: any) => context.gameData,
                          clueWords: (context: any, event: any) => O.none,
                        },
                      }
                    },
                    [crosswordStates.TPuzzleStates.puzzles_loading_error]: {
                      type: 'final'
                    }
                  },
                }
              },
              on: {
                [crosswordEvents.TCategoryEvents.SELECT_CATEGORY]: {
                  target:   crosswordStates.TCategoryStates.selectCategory,
                  actions:  crosswordActions.TCategoriesActions.assignActiveCategory,
                }
              }
            },
          },
          on: {
            [languageEvents.TLanguageEvents.SELECT_CROSSLANG]: {
              target:   crosswordStates.TCrosswordStates.selectCrossLang,
              actions:  crosswordActions.TCrosswordLanguageSelectActions.assignActiveCrossLang,
            }
          }
        },
      }
    }
  }
}
 


