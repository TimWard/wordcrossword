//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import { ICategory, CATEGORIES } from 'crosstypes';

export const createDefaultCategory =
(): ICategory => 
  {return {id: CATEGORIES.BEGINNER, name: 'Default Cat name', description: 'Default Cat Desc'}}

export const createErrorCategory =
(desc: string): ICategory => 
  {return {id: CATEGORIES.ERRORCAT, name: 'Failed to load. Could not contact the server', description: 'Error Cat Desc'}}
  
  
const getProperyByCategory = 
   (category: ICategory) =>
   (values: number[])
   : number => 
   category.id === CATEGORIES.BEGINNER
   ? values[0]
   :category.id === CATEGORIES.INTERMEDIATE
     ? values[1]
     : category.id === CATEGORIES.ADVANCED
       ? values[2]
       : values[3]

export const getMaxWordLength = 
   (category: ICategory)
   : number => 
  getProperyByCategory (category) ([7, 9, 11, 15])

export const getRank = 
   (category: ICategory)
   : number => 
  getProperyByCategory (category) ([1000, 2000, 5000, 0])

         
