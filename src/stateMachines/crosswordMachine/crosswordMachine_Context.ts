//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// My functions
import * as fp_Category from '../../Categories/fp_Category'
import * as fp_Language from '../../Languages/fp_Language';
import * as fp_Utils from "../../Game/fp_Utils";
import * as fp_Websitetexts from '../../Websitetext/fp_Websitetexts';

import { gameData } from 'dblib';

import { ICrosswordContext } from './crosswordTypes_Context'
import { createEmptyPuzzle } from 'crosstypes';

export const crosswordContext: ICrosswordContext = {
  //  siteLangId: 4,
    // Todo - pass this into the cross lang machine
    dayIndex: fp_Utils.getDayIndexFromURL (),

    browserLang: '',
    screenWidth: 1000,

    activePuzzle: createEmptyPuzzle(),

    // Todo - can we get rid of this?
    gameData: gameData,

    categories: [fp_Category.createDefaultCategory()],
    activeCategory: fp_Category.createDefaultCategory(),
  
    crossLangs: [fp_Language.createDefaultCrossLanguage()],
    activeCrossLang: fp_Language.createDefaultCrossLanguage(),
  
    clueLangs: [fp_Language.createDefaultLanguage()],
    activeClueLang: fp_Language.createDefaultLanguage(),
  
    siteLangs: [fp_Language.createDefaultLanguage()],
    activeSiteLang: fp_Language.createDefaultLanguage(),
  
    websitetexts: [fp_Websitetexts.createEmptyWebsitetext ()],
  }

  