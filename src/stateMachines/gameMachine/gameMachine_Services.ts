//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { ServiceConfig} from 'xstate'

import { IGameContext } from './gameMachineTypes_Context';

import * as crosswordMachineDB from '../crosswordMachine/crosswordMachineDB';
import * as gameMachineServices from './gameMachineTypes_Services';

export const gameServices: Record <string, ServiceConfig <IGameContext>> = 
{
  [gameMachineServices.TClueWordsServices.cluewords_loading_svc]: (context, _) => 
    crosswordMachineDB.loadWordsForClues 
    (context.gameData.wordData) (context.activePuzzle.id) (context.activeClueLang.id),


  [gameMachineServices.TGridServices.grids_loading_svc]: (context, _) => 
    crosswordMachineDB.loadGrid 
      (context.gameData.gridData) (context.gameData.squareData) 
      (context.activePuzzle) ,
}

