//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//----------------------------------------
// These types are used by the e2e testing
// They should match the data-testid in the tsx files
//----------------------------------------


// Todo - add an id for the feedback button
export const TCrosswordButtonIds = {
//  crosswords_button: 'crosswords-button',
  crosswords_button: 'should_error',
  dictionary_button: 'dictionary-button',
}

// Todo - add an id for the feedback form
export const TCrosswordFormIds = {
  crosswords_form: 'crosswords-form',
  dictionary_form: 'dictionary-form',
}


