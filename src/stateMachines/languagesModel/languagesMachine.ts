//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { EventObject, MachineConfig } from 'xstate'
//import { actions } from 'xstate'

// My machine types
import { TMachineIds } from '../crosswordMachineIds';
import { TLanguageServices, TLanguageActions, TClueLangStates, TCrossLangStates, TLanguageMachineStates, TSiteLangStates } from './languageTypes';
import { ILanguage } from 'crosstypes';
import { ILanguageData } from 'dblib';

export interface ILoadingContext {
  languageData: ILanguageData
  // Supplied by the parent
  siteLangId: number
  dayIndex: number
  // Loaded by this machine
  crossLangs: ILanguage[]
  clueLangs: ILanguage[]
  siteLangs: ILanguage[]
}


export type TLoadingMachineConfig = MachineConfig <ILoadingContext, any, EventObject>


export const loadingMachineConfig: TLoadingMachineConfig = {
  id: TMachineIds.loadingMachine,
  initial: TLanguageMachineStates.loading,
  states: {
    [TLanguageMachineStates.loading]: {
      type: 'parallel',
      states: {
        [TLanguageMachineStates.crosslang_loader]: {
          initial: TCrossLangStates.loading,
          states: {
            [TCrossLangStates.loading]: {
              invoke: {
                id: 'crosslang_loading_Id',
                src: TLanguageServices.crosslang_loading,
                onDone: {
                  target: TCrossLangStates.loaded,
                  actions: TLanguageActions.assignCrossLangs,
                },
                onError: {
//                  actions: TLanguageActions.assignLangsError
//                  actions: actions.escalate ({ message: 'Error loading cross langs' }),
                }
              }
            },
            [TCrossLangStates.loaded]: {
              onEntry: () => undefined,//console.log ('Reached TCrossLangStates.loaded'),
              type: 'final'
            },
          }
        },
        [TLanguageMachineStates.cluelang_loader]: {
          initial: TClueLangStates.loading,
          states: {
            [TClueLangStates.loading]: {
              onEntry: () => undefined, //console.log ('Reached TClueLangStates.loading'),
              invoke: {
                id: 'cluelang_loading_Id',
                src: TLanguageServices.cluelang_loading,
                onDone: {
                  target: TClueLangStates.loaded,
                  actions: TLanguageActions.assignClueLangs,
                },
                onError: {
                  // This will be sent to the parent machine that invokes this child
//                  actions: actions.escalate ({ message: 'Error loading clue langs' }),
    //              actions: TLanguageActions.assignLangsError
                }
              }
            },
            [TClueLangStates.loaded]: {
              onEntry: () => undefined, //console.log ('Reached TClueLangStates.loaded'),
              type: 'final',
              data: {
                clueLangs: (context: any, event: any) => context.clueLangs
              }
            },
          }
        },
        [TLanguageMachineStates.sitelang_loader]: {
          initial: TSiteLangStates.loading,
          states: {
            [TSiteLangStates.loading]: {
              onEntry: () => undefined, //console.log ('Reached TSiteLangStates.loading'),
              invoke: {
                id: 'sitelang_loading_Id',
                src: TLanguageServices.sitelang_loading,
                onDone: {
                  target: TSiteLangStates.loaded,
                  actions: TLanguageActions.assignSiteLangs,
                },
                onError: {
                  // This will be sent to the parent machine that invokes this child
//                  actions: actions.escalate ({ message: 'Error loading site langs' }),
    //              actions: TLanguageActions.assignLangsError
                }
              }
            },
            [TSiteLangStates.loaded]: {
              type: 'final',
              data: {
                siteLangs: (context: any, event: any) => context.siteLangs
              }
            },
          }
        },
      },
      onDone: TLanguageMachineStates.loaded
    },
    [TLanguageMachineStates.loaded]: {
      onEntry: () => undefined, //console.log ('Reached [TLanguageMachineStates.loaded]'),
      type: 'final',
      data: {
        crossLangs: (context: ILoadingContext, event: any) => context.crossLangs,
        clueLangs: (context: ILoadingContext, event: any) => context.clueLangs,
        siteLangs: (context: ILoadingContext, event: any) => context.siteLangs,
      },
    }
  }
}  

