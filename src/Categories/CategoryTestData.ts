//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import * as TE from 'fp-ts/lib/TaskEither'

import { ICategoryData } from "dblib";
import { ICategory } from "crosstypes";

const loadCategoriesFromDB =
(languageId: number) =>
(dayIndex: number)
: TE.TaskEither <Error, ICategory[]> => 
  TE.right ([
    {id: 9, name: 'Test Cat1 Name', description: 'Test Cat1 Desc'},
//    {id: 10, name: 'Test Cat2 Name', description: 'Test Cat2 Desc'}
  ])


export const categoryTestData: ICategoryData = {
  loadCategoriesFromDB,
}
