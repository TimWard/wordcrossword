//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// Other 3rd party imports
import produce from 'immer';

// fp-ts imports
import * as E from "fp-ts/lib/Either";
import * as T from 'fp-ts/lib/Task'
import { pipe } from 'fp-ts/lib/pipeable';
import * as A from 'fp-ts/lib/Array'

// DB Interfaces
import { IGameData } from "dblib";
import { IWordData } from 'dblib';
import { IGridData } from "dblib";
import { ISquareData } from "dblib";


// Type interfaces
import { IPuzzle, ICategory, IWebsitetext, IWord } from "crosstypes";
import { logging } from "crosstypes";

import * as fp_PuzzlesIO from '../../Puzzles/fp_PuzzlesIO';
import {cf_Grids} from 'crossfuncs';
import * as fp_Clues from '../../Clues/fp_Clues';




// Todo - test that this fails correctly!
export const loadCategories = 
(gameData: IGameData) =>
(crossLangId: number) =>
(dayIndex: number):
Promise <ICategory[]> =>
  gameData.categoryData.loadCategoriesFromDB (crossLangId) (dayIndex) ().then (
  E.fold (
    err => {throw new Error (err.message)},
    categories => categories
  )
)


export const loadWebsitetext = 
(gameData: IGameData) =>
(activeSiteLangId: number):
Promise <IWebsitetext[]> =>
  gameData.websitetextData.loadWebsitetextFromDB (activeSiteLangId) ()

  
export const loadWordsForClues = 
(wordData: IWordData) =>
(puzzleId: number) =>
(activeClueLangId: number):
Promise <IWord[]> =>
  wordData.loadWordsByPuzzleFromDB (activeClueLangId) (puzzleId) ()


export const loadPuzzles = 
  (gameData: IGameData) =>
  (activeCrossLangId: number) => 
  (activeCrossLangCode: string) => 
  (activeCrossLangAlphabetId: number) => 
  (activeCategoryId: number ) =>
  (dayIndex: number)
  : Promise <IPuzzle[]> => 
      fp_PuzzlesIO.loadPuzzlesForLangAndCategory 
        (gameData)
        (activeCrossLangId) 
        (activeCrossLangCode) 
        (activeCrossLangAlphabetId)
        (activeCategoryId)
        (dayIndex)
        ()

export const loadGrid =
(gridData: IGridData) =>
(squareData: ISquareData) =>
(activePuzzle: IPuzzle):
Promise <IPuzzle> =>
{
  //  console.log ('loadGrid: activePuzzle = ')
  //  console.log (activePuzzle)
  let result =  pipe (
      cf_Grids.loadAndPopulateGrid
        (gridData) 
        (squareData) 
        (activePuzzle.gridid),
      T.map (logging.typedPipeableLogTextOnly (3, 'Just called loadAndPopulateGrid')),
      T.map ( logging.typedPipeableLog (3, 'grid = ') ),
      
      T.map (grid => pipe (
        // This updates the clues to use the generated blankwords
        activePuzzle.clues,
        A.map (fp_Clues.assignBlankwordToClue (grid.blankwords)), 
        fp_Clues.sortByDirectionByClueNo,
        clues => produce (activePuzzle, draft => {draft.clues = clues} ),
        newPuzzle => produce (newPuzzle, draft => {draft.grid = grid} ),
        newPuzzle => produce (newPuzzle, draft => {
          fp_Clues.updateSquaresForAllClues  (draft.grid.squares) (newPuzzle.clues)
        }),
      )),
    ) ()
    // console.log ('leaving loadGrid: activePuzzle = ')
    // console.log (result)

    return result
   }
