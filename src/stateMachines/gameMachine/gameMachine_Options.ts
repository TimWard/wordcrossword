//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { State, EventObject,MachineOptions, AnyEventObject, StateMachine, createMachine } from 'xstate'


// My functions
import { gameMachineConfig } from './gameMachine';

import { IGameContext } from './gameMachineTypes_Context';

import { gameContext } from './gameMachine_Context';
import { gameActions } from './gameMachine_Actions';
import { gameServices } from './gameMachine_Services';


const gameMachineOptions: Partial<MachineOptions<IGameContext, AnyEventObject>> =
{
  services: gameServices,
  actions: gameActions,
}


export type IGameMachine = StateMachine <IGameContext, any, EventObject>
export type TGameState = State <IGameContext, EventObject, any>

export const gameMachine: IGameMachine = 
  createMachine <IGameContext, any, TGameState> 
    (gameMachineConfig, gameMachineOptions)
      .withContext (gameContext)


      
      