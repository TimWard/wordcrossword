//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

export const TPlayMachineStates = {
  play_wrapper:             'play_wrapper',
  crossword_states:         'crossword_states',
  game_over:                'game_over',

  crossword_editing:        'crossword_editing',
  crossword_complete:       'crossword_complete',

  clue_editing:             'clue_editing',
  clue_final:               'clue_final',

  square_editing:           'square_editing',
  square_final:             'square_final',
  
  reveal_all_confirm:       'reveal_all_confirm',
} 

export const TTitleAnimationStates = {
  playgame_title_animation:         'playgame_title_animation',
  playgame_title_animation_start:   'playgame_title_animation_start',
  playgame_title_animation_done:    'playgame_title_animation_done',
}

