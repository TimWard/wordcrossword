//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// 3rd-party files
import * as React from 'react'
//import CookieConsent from 'react-cookie-consent'
// import * as cssUtils from '../Utilities/cssUtils';


// xstate import
import { useMachine, useActor } from "@xstate/react";

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';
import * as O from 'fp-ts/lib/Option'

// interface imports
import {ISendCrosswordMachineState} from './GameStateInterface';
import { IGameData } from 'dblib';

import {buildConfig} from "../myConfig"
import {BUILD_TARGETS} from 'dblib';

// Pure fp imports
import * as fp_gamePure from './fp_gamePure';

// dirty imports
import * as fp_Screen from '../Screen/fp_Screen';

// TSX files
import {WebsitetextElement, WebsitetextExtra} from '../Websitetext/Websitetext';
import {Dictionary} from '../Dictionary/Dictionary';
import {Feedback} from '../Feedback/Feedback';
import {Grid} from '../Grids/Grid';
import {Clues} from '../Clues/Clues';
import {SingleClue} from '../Clues/SingleClue';
import {CategoriesElement} from '../Categories/Categories';
import {BoardButtons, BoardButtonSkip320, BoardButtonReveal320} from '../BoardButton/BoardButtons';
import { GameTitle } from '../GameTitle/GameTitle';
import {Languages} from '../Languages/Languages';
import { GamePageButtons } from '../GamePageButtons/GamePageButtons';

// Crossword machine
import { crosswordMachine } from '../stateMachines/crosswordMachine/crosswordMachine_Options';
import { TCrosswordState } from "../stateMachines/crosswordMachine/crosswordTypes";
import * as machineStateUtils from '../stateMachines/crosswordStateUtils';

// Game machine
import { ISendGameState } from '../stateMachines/gameMachine/gameMachineTypes';
import * as gameStateUtils from '../stateMachines/gameStateUtils';

// Machine Ids
import { TInvokedMachineIds } from '../stateMachines/crosswordMachineIds';
import { TCrosswordFormIds } from '../stateMachines/crossswordUiIds';
import { TGameState } from '../stateMachines/gameMachine/gameMachine_Options';
import { TPlayState } from '../stateMachines/playMachine/playMachine_Options';
import { ISendPlayState } from '../stateMachines/playMachine/playMachineTypes';


interface IGameProps {
  isScreenWidth768: boolean
  gameData: IGameData
}

type IGetTranslatedText = (englishText: string) => string
 

// React.memo (
export const Game = (props: IGameProps) => {

  // The devTools option makes the statechart viewable in Chrome
  const devToolsOption = 
    buildConfig.target === BUILD_TARGETS.LIVE
      ? { devTools: true }
      : { devTools: false }

  // Todo - replace this after demoing to companies
  const [crosswordState, sendCrosswordState] = useMachine (crosswordMachine, { devTools: true })
//  const [crosswordState, sendCrosswordState] = useMachine (crosswordMachine, devToolsOption)

  const gameActor  = crosswordState.children[TInvokedMachineIds.gameMachine]

// Todo - replace any with ISendGameState
//  const [gameState, sendGameState]: [TGameState, ISendGameState] = useActor (gameActor)
  const [gameState, sendGameState]: [TGameState, any] = useActor (gameActor)

  const playActor = gameState 
    ? gameState.children[TInvokedMachineIds.playMachine]
    : crosswordState.children[TInvokedMachineIds.playMachine]
    
  const [tempPlayState, sendPlayState]: [TPlayState, any] = useActor (playActor)
  const playState: O.Option <TPlayState> = O.fromNullable (tempPlayState)


  const getTranslatedText: IGetTranslatedText = 
    React.useCallback (fp_gamePure.getTranslatedText 
        (crosswordState.context.websitetexts), 
        [machineStateUtils.hasWebsitetextLoaded (crosswordState)])
  


  // Todo - should check that all the elements have loaded
  // Each element should be only be rendered if all the components
  // on which it is dependent have already loaded
  // return hasGridLoaded (machineState)
  // <div> 
  return <div> 
      {machineStateUtils.isOnDictionaryPage (crosswordState)
        ? renderDictionaryPage (props) (crosswordState, sendCrosswordState) (gameState, sendGameState) (playState, sendPlayState) (getTranslatedText)
        : machineStateUtils.isOnFeedbackPage (crosswordState)
          ? renderFeedbackPage (props) (crosswordState, sendCrosswordState) (gameState, sendGameState) (playState, sendPlayState) (getTranslatedText)
          : props.isScreenWidth768
            ? renderCrosswordPage (props) (crosswordState, sendCrosswordState) (gameState, sendGameState) (playState, sendPlayState) (getTranslatedText)
            : renderCrosswordPage320 (props) (crosswordState, sendCrosswordState) (gameState, sendGameState) (playState, sendPlayState)  (getTranslatedText)
      }
  </div>
}


//-----------------------------------------------------------------------------
// Utility Functions for rendering
//-----------------------------------------------------------------------------

const conditionalRender = 
(fn: Function, predicate: boolean) => 
  predicate
    ? fn ()
    : ""

const renderTitleSection = 
(machineState: TCrosswordState, sendMachineState: ISendCrosswordMachineState) =>
(playState: O.Option <TPlayState>, sendPlayState: ISendPlayState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 
  <div>
    <div className="titlerow">
      {renderTitleLeft ()}
      {renderTitle (sendPlayState) (playState) ()}
      {renderTitleRight (sendMachineState) (machineState) (getTranslatedText) ()}
    </div>
  </div>

//-----------------------------------------------------------------------------
// Functions for rendering the languages and categories section
//-----------------------------------------------------------------------------

const renderLanguagesAndCategories = 
(props: IGameProps) =>
(machineState: TCrosswordState, sendMachineState: ISendCrosswordMachineState) =>
(gameState: TGameState, sendGameState: ISendGameState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 
  <div className="game-options">
    {renderLanguages (props) (machineState, sendMachineState) (gameState, sendGameState) (getTranslatedText)}
    {renderCategories (sendMachineState) (machineState, props) (getTranslatedText)}
  </div>


const renderLanguages = 
(props: IGameProps) =>
(machineState: TCrosswordState, sendMachineState: ISendCrosswordMachineState) =>
(gameState: TGameState, sendGameState: ISendGameState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 
  <Languages 
    languageData      = {props.gameData.languageData}

    getTranslatedText = {getTranslatedText}
    sendMachineState  = {sendMachineState}
    sendGameState     = {sendGameState}

    isScreenWidth768  = {props.isScreenWidth768}

    browserLang       = {machineState.context.browserLang}
    activeSiteLang    = {machineState.context.activeSiteLang}
    activeClueLang    = {machineState.context.activeClueLang}
    activeCrossLang   = {machineState.context.activeCrossLang}
    crossLangs        = {machineState.context.crossLangs}
    clueLangs         = {machineState.context.clueLangs}
    siteLangs         = {machineState.context.siteLangs}
  />

  
  
  

const renderCategories = 
(sendMachineState: ISendCrosswordMachineState) =>
(machineState: TCrosswordState, props: IGameProps) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 
  <CategoriesElement 
    getTranslatedText = {getTranslatedText}
    sendMachineState  = {sendMachineState}
    activeCategoryId  = {machineState.context.activeCategory.id}
    categories        = {machineState.context.categories}
    activeCrossLangId = {machineState.context.activeCrossLang.id}  
    WebsitetextElement = {WebsitetextElement}    
 />

//-----------------------------------------------------------------------------
// Functions for rendering the clues section
//-----------------------------------------------------------------------------


const renderClues = 
(props: IGameProps) =>
(machineState: TCrosswordState, sendMachineState: ISendCrosswordMachineState) =>
(gameState: TGameState, sendGameState: ISendGameState) =>
(playState: TPlayState, sendPlayState: ISendPlayState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 

  <div className="game-clues-wrapper">
    <Clues
      playState         = {playState}
      sendPlayState     = {sendPlayState}
      getTranslatedText = {getTranslatedText}
      activeClueLangId  = {machineState.context.activeClueLang.id}
    />
    <div className="board-buttons">
      {conditionalRender (renderButtons (playState, sendPlayState) (machineState) (getTranslatedText), 
        (props.isScreenWidth768))}
    </div>
  </div>


const renderActiveClueOnly =
(playState: TPlayState)
: JSX.Element => 
  <SingleClue
    activeClue  = {playState.context.activeClue}
    clueWords   = {playState.context.clueWords}
  />

//-----------------------------------------------------------------------------
// Functions for rendering the right hand side buttons section
//-----------------------------------------------------------------------------

// Note that this is lazy, so that it can be used in a conditional render
const renderButtons = 
(playState: TPlayState, sendPlayState: ISendPlayState) =>
(machineState: TCrosswordState) => 
(getTranslatedText: IGetTranslatedText) =>
()
: JSX.Element => 

  <BoardButtons
    getTranslatedText = {getTranslatedText}
    playState         = {playState}
    sendPlayState     = {sendPlayState}
    siteLangId        = {machineState.context.activeSiteLang.id}
  />


//-----------------------------------------------------------------------------
// Functions for rendering the title
//-----------------------------------------------------------------------------


// Note that this is lazy
const renderTitle = 
(sendPlayState: ISendPlayState) =>
(playState: O.Option <TPlayState>) =>
()
: JSX.Element => 

  pipe (
    playState,
    O.fold (
      () => <div/>,
      playState => <GameTitle  
        titleLetters        = {playState.context.titleLetters}
        titleAnimationCss   = {playState.context.titleAnimationCss}
        sendPlayState       = {sendPlayState}
      />
    )
  )

    



// Note that this is lazy
const renderTitleLeft = () =>
<div 
  className="titlerowleft">
</div>

// Note that this is lazy
const renderTitleRight =
(sendMachineState: ISendCrosswordMachineState) =>
(machineState: TCrosswordState) => 
(getTranslatedText: IGetTranslatedText) => ()
: JSX.Element => 
  <div className="titlerowright">
    {renderTitleRightButtons (sendMachineState) (machineState) (getTranslatedText)}
  </div>


const renderTitleRightButtons = 
(sendMachineState: ISendCrosswordMachineState) =>
(machineState: TCrosswordState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 
    <GamePageButtons
      sendMachineState                  = {sendMachineState}
      getTranslatedText                 = {getTranslatedText}
      machineState                      = {machineState}
      siteLangId                        = {machineState.context.activeSiteLang.id}
    />


//-----------------------------------------------------------------------------
// Functions for rendering the dictionary page
//-----------------------------------------------------------------------------

const renderGrid = 
(props: IGameProps) =>
(playState: TPlayState, sendPlayState: ISendPlayState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 

  <div>
    <div className="board">
      <Grid
        sendPlayState             = {sendPlayState}
        activePuzzle              = {playState.context.activePuzzle}
        activeClue                = {playState.context.activeClue}
        activeSquare              = {playState.context.activeSquare}
        screenWidth               = {fp_Screen.getScreenWidth ()}
        activeCategoryId          = {playState.context.activeCategory.id}
      />
    </div>
    <br></br>
      {conditionalRender (renderPointsSubTitle (playState) (getTranslatedText), (props.isScreenWidth768))}
    {/* <div>
      {conditionalRender (renderCookieConsent, (props.isScreenWidth768))}
    </div> */}
  </div>


// Todo - reinstate this section when it works on all screen sizes
// // This is lazy so that it can be used in conditional renderings
// const renderCookieConsent = () =>
//   <CookieConsent 
//     buttonText='ACCEPT' 
//     enableDeclineButton
//     declineButtonText = 'DECLINE'
//   >
//     This website uses cookies
//   </CookieConsent>

// This is lazy so that it can be used in conditional renderings
const renderPreGameSubTitle = 
(getTranslatedText: IGetTranslatedText) => ()
: JSX.Element => 
  <div className="subtitlerow">
    <b><WebsitetextElement 
      getTranslatedText = {getTranslatedText} 
      text = {'New puzzles every day'}
    />: </b>
  </div>

// This is lazy so that it can be used in conditional renderings
const renderPointsSubTitle = 
(playState: TPlayState) =>
(getTranslatedText: IGetTranslatedText) => ()
: JSX.Element => 
  <div className="subtitlerow">
    <b><WebsitetextExtra 
      getTranslatedText = {getTranslatedText} 
      text = {'Points'}
      additionalText = {': ' + playState.context.points}
    /></b>
  </div>


//-----------------------------------------------------------------------------
// Functions for rendering the feedback page
//-----------------------------------------------------------------------------

const renderFeedbackPage = 
(props: IGameProps) =>
(machineState: TCrosswordState, sendMachineState: ISendCrosswordMachineState) =>
(gameState: TGameState, sendGameState: ISendGameState) =>
(playState: O.Option <TPlayState>, sendPlayState: ISendPlayState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 

  <div className="game-total" data-testid={TCrosswordFormIds.dictionary_form}>
    <div className="game-wrapper">  
    <div className="game">
        <div style={{width: "1%"}}>
        </div>
        <div className="game-options">
        </div>
     </div>
     <div className="dictionary-board">
     <Feedback
        feedbackData = {props.gameData.feedbackData}
        getTranslatedText = {getTranslatedText}              
      />
    </div>
    </div>
    <div className="game-title"> 
      {renderTitleRight (sendMachineState) (machineState) (getTranslatedText) ()}
    </div>
  </div>


//-----------------------------------------------------------------------------
// Functions for rendering the dictionary page
//-----------------------------------------------------------------------------

// Todo - reinstate the dictionary page when it works on all screen sizes

const renderDictionaryPage = 
(props: IGameProps) =>
(machineState: TCrosswordState, sendMachineState: ISendCrosswordMachineState) =>
(gameState: TGameState, sendGameState: ISendGameState) =>
(playState: O.Option <TPlayState>, sendPlayState: ISendPlayState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 
  <div className="game-total" data-testid={TCrosswordFormIds.dictionary_form}>
    <div className="game-title"> 
      {renderTitleSection (machineState, sendMachineState) (playState, sendPlayState) (getTranslatedText)}
    </div>
    <div className="game-wrapper">  
      <div className="game">
        <div style={{width: "1%"}}>
        </div>
        <div className="game-options">
          {renderLanguages (props) (machineState, sendMachineState) (gameState, sendGameState) (getTranslatedText)}
          {renderCategories (sendMachineState) (machineState, props) (getTranslatedText)}
        </div>
        <div className="dictionary-board">
          <Dictionary 
            activeCategory    = {machineState.context.activeCategory}
            activeClueLang    = {machineState.context.activeClueLang}
            activeCrossLang   = {machineState.context.activeCrossLang}
            getTranslatedText = {getTranslatedText}              
            wordData          = {props.gameData.wordData}
          />
        </div>
      </div>
    </div>
  </div>

//-----------------------------------------------------------------------------
// Functions for rendering the crossword page
//-----------------------------------------------------------------------------


const renderCrosswordPage = 
(props: IGameProps) =>
(crosswordState: TCrosswordState, sendMachineState: ISendCrosswordMachineState) =>
(gameState: TGameState, sendGameState: ISendGameState) =>
(playState: O.Option <TPlayState>, sendPlayState: ISendPlayState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 
  <div className="game-total">
    <div className="game-title"> 
      {renderTitleSection (crosswordState, sendMachineState) (playState, sendPlayState) (getTranslatedText)}
    </div>
    <div className="game-wrapper"> 
      <div className="game">
        <div style={{width: "1%"}}>
        </div>
        {renderLanguagesAndCategories (props) (crosswordState, sendMachineState) (gameState, sendGameState) (getTranslatedText)}
        <div className="game-board">
          {
            pipe (
              playState,
              O.fold (
                () => undefined,
                playState => renderGrid (props) (playState, sendPlayState) (getTranslatedText)
              )
            )
          }
        </div>
        {
        pipe (
          playState,
          O.fold (
            () => undefined,
            playState => gameStateUtils.hasPlayMachineLoaded (playState)
              ? renderClues (props) (crosswordState, sendMachineState) (gameState, sendGameState) (playState, sendPlayState) (getTranslatedText)
              : undefined
          )
        )
      }
        <div style={{width: "1%"}}>
        </div>
      </div>
    </div>
  </div>

//-----------------------------------------------------------------------------
// Functions for rendering the buttons320 section
//-----------------------------------------------------------------------------

const renderButtonSkip320 = 
(playState: TPlayState, sendPlayState: ISendPlayState) =>
(machineState: TCrosswordState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 

  pipe (
    { getTranslatedText:  getTranslatedText,
      playState:          playState,
      sendPlayState:      sendPlayState,
      siteLangId:         machineState.context.activeSiteLang.id},
    props => <div>
      <BoardButtonSkip320 
        props = {props}
      />
    </div>
  )

const renderButtonReveal320 = 
(playState: TPlayState, sendPlayState: ISendPlayState) =>
(machineState: TCrosswordState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 

  pipe (
    { getTranslatedText:  getTranslatedText,
      playState:          playState,
      sendPlayState:      sendPlayState,
      siteLangId:         machineState.context.activeSiteLang.id},
    props => <div>
      <BoardButtonReveal320 
        props = {props}
      />
    </div>
  )


const renderCrosswordPage320 = 
(props: IGameProps) =>
(machineState: TCrosswordState, sendMachineState: ISendCrosswordMachineState) =>
(gameState: TGameState, sendGameState: ISendGameState) =>
(playState: O.Option <TPlayState>, sendPlayState: ISendPlayState) =>
(getTranslatedText: IGetTranslatedText)
: JSX.Element => 
<div className="game-total">
  <div> 
    {
      pipe (
        playState,
        O.fold (
          () => renderPreGameSubTitle (getTranslatedText) (),
          playState => renderPointsSubTitle (playState) (getTranslatedText) ()
        )
      )
    }
  </div> 
  <div className="game-wrapper"> 
    <div className="game">
      <div style={{width: "1%"}}>
      </div>
      <div className="gameBoard">
      {
        pipe (
          playState,
          O.fold (
            () => undefined,
            playState => renderGrid (props) (playState, sendPlayState) (getTranslatedText)
          )
        )
      }
      </div>
      <div style={{width: "1%"}}>
      </div>
    </div>
    <div className="clueAndBoardButtons">
      {
        pipe (
          playState,
          O.fold (
            () => undefined,
            playState => renderButtonSkip320 (playState, sendPlayState) (machineState) (getTranslatedText)
          )
        )
      }
      {
        pipe (
          playState,
          O.fold (
            () => undefined,
            playState => renderActiveClueOnly (playState)
          )
        )
      }
      {
        pipe (
          playState,
          O.fold (
            () => undefined,
            playState => renderButtonReveal320 (playState, sendPlayState) (machineState) (getTranslatedText)
          )
        )
      }
    </div>
  <div className="game-title"> 
    {conditionalRender (renderTitle (sendPlayState) (playState), (true))}
  </div>
  </div>
  <div>
    {renderLanguages (props) (machineState, sendMachineState) (gameState, sendGameState) (getTranslatedText)}
    <div className="titlerow">
      {renderCategories (sendMachineState) (machineState, props) (getTranslatedText)}
      {renderTitleRight (sendMachineState) (machineState) (getTranslatedText) ()}
    </div>
  </div>
</div>


//-----------------------------------------------------------------------------
// End of - Functions for rendering the buttons320 section
//-----------------------------------------------------------------------------


