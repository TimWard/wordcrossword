//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import { ILanguage } from 'crosstypes';

import * as languageEvents from '../languageTypes_Events';


export type TSelectClueLangEvent = 
{
  type: typeof languageEvents.SELECT_CLUELANG
  value: ILanguage
}



export type TGameEventTypes = 
  TSelectClueLangEvent

