//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//-------------------------------------------------------------------------
// Having these string constants defined here allows them
// to be treated as types 
// Not sure how typescript works in this respect, 
// but this is the strongest form of typing I've managed to achieve
// Even if it is a bit convoluted, strong typing prevents nasty runtime errors!
// It also eliminates duplication of strings
//-------------------------------------------------------------------------


// Page events
// ------------------------------------------
export const CLICK_CROSSWORDS     = 'CLICK_CROSSWORDS'
export const CLICK_DICTIONARY     = 'CLICK_DICTIONARY'
export const CLICK_FEEDBACK       = 'CLICK_FEEDBACK'

export const TPageEvents = {
  CLICK_CROSSWORDS:   CLICK_CROSSWORDS,
  CLICK_DICTIONARY:   CLICK_DICTIONARY,
  CLICK_FEEDBACK:     CLICK_FEEDBACK,
} 

// Categories
// ------------------------------------------
export const SELECT_CATEGORY     = 'SELECT_CATEGORY'
export const TCategoryEvents = {
  SELECT_CATEGORY:    SELECT_CATEGORY,
} 

