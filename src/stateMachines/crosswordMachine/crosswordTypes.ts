//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { State, MachineConfig } from 'xstate'

import { ICrosswordContext } from './crosswordTypes_Context';
import { TCrosswordEventTypes } from './crosswordMachine_Events';



export type TCrosswordState = State <ICrosswordContext, TCrosswordEventTypes, any>

export type TCrosswordMachineConfig = MachineConfig <ICrosswordContext, any, TCrosswordEventTypes>
