module.exports = {
  preset: 'jest-puppeteer',
//  preset: 'ts-jest',
//  testEnvironment: 'node',
  testPathIgnorePatterns: ['/node_modules/', 'dist'],
  transform: {"^.+\\.ts?$": "ts-jest"},
//  testMatch: [ "**/__tests__/**/*.[jt]s?(x)", "**/?(*.)+(spec|test).[jt]s?(x)" ],
//  testMatch: ['**/e2e/*.e2e.ts'],
  testMatch: ['**/__e2e__/*.e2e.ts'],
  moduleFileExtensions: [
    "tsx",
    "ts",
    "js",
    "jsx",
    "json",
    "node"
  ],
  collectCoverage: true,
  "globalSetup": "jest-environment-puppeteer/setup",
  "globalTeardown": "jest-environment-puppeteer/teardown",
  "testEnvironment": "jest-environment-puppeteer"
};
