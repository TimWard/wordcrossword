//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
//import * as cssUtils from '../Utilities/cssUtils';

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';
import * as O from 'fp-ts/lib/Option'
import * as A from 'fp-ts/lib/Array'

// JSX
import { Clue } from './Clue';

// Classes
import {crDOWN, crACROSS, TDirection} from 'crosstypes';
import { IClue, IWord, Clue as ClueClass } from 'crosstypes';

// Pure fp imports
import * as fp_Clue from './fp_Clue';
import {cf_Blankword} from "crossfuncs";
import * as fp_Squares from '../Squares/fp_Squares';

// Interface to the state machine
import * as playEvents from '../stateMachines/playMachine/playMachineTypes_Events';
import { ISendPlayState } from '../stateMachines/playMachine/playMachineTypes';
import { TPlayState } from '../stateMachines/playMachine/playMachine_Options';


interface ICluesProps {
  playState: TPlayState
  // Callbacks
	sendPlayState: ISendPlayState
  getTranslatedText: (englishText: string) => string
  // For testing only
  activeClueLangId: number
}

  
const getDisplayText = (clue: IClue, clueWords: O.Option <IWord[]>) =>
  pipe (
    clueWords,
    O.fold ( 
      () => '',
      fp_Clue.displayText (clue) 
    )
  )

export const Clues = 
React.memo (
(props: ICluesProps) => { 

  const renderClue = 
  (clue: IClue, move: number) =>
  (isActiveClue: boolean) =>
  
  
    <Clue
      move        = {move}
      clueNo      = {ClueClass.clueNo (clue)}
      isActive    = {isActiveClue && !fp_Squares.isCrosswordCorrect (props.playState.context.activePuzzle.grid.squares)}
      displayText = {getDisplayText (clue, props.playState.context.clueWords)}
      onClueClick = {() => props.sendPlayState ({type: playEvents.SELECT_CLUE, value: clue})}
      isCorrect   = {cf_Blankword.isCorrect (props.playState.context.activePuzzle.grid.squares) (clue.blankword)}
    />


  const renderClueWithIndex = 
  (move: number, clue: IClue)  =>
 
    pipe (
      fp_Clue.isActiveClue (props.playState.context.activeClue) (clue),
      renderClue (clue, move) 
    )

  const renderCluesForDirection = 
  (direction: TDirection)  =>
  
    pipe (
      props.playState.context.activePuzzle.clues,
      A.filter (clue => ClueClass.direction (clue) === direction),
      A.mapWithIndex (renderClueWithIndex)
    )


  const doRenderCluesForDirection = 
  (direction: TDirection, directionText: string)  =>

    <div className="cluelistfordirection" >
      <hr className = "cluesheaderline1"/>
      <b>
        {props.getTranslatedText (directionText)}
      </b>
      <hr className = "cluesheaderline2"/>
      <ol>
        {renderCluesForDirection (direction)}
      </ol>
    </div>

  // Commented out line used for testing updates with React.memo
  // <div style = {{color:  cssUtils.getRandomColor ()}}>Clue lang: {props.activeClueLangId}</div>
      
  // Note that return has to be on the same line as the <div 
  return  <div className="game-clues">
            {doRenderCluesForDirection (crACROSS, 'Across')}
            <div className="cluescentralreservation">
            </div>
            {doRenderCluesForDirection (crDOWN, 'Down') }
          </div>

}
)

