//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

export const TCategoriesServices = {
  categories_loading_svc:       'categories_loading_svc',
} 

export const TWebsitetextServices = {
  websitetext_loading_svc:       'websitetext_loading_svc',
} 

export const TPuzzleServices = {
  puzzles_loading_svc:       'puzzles_loading_svc',
} 




