//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

export const TCategoriesActions = {
  assignCategories:         'assignCategories',
  assignCategoriesError:    'assignCategoriesError',
  assignActiveCategory:     'assignActiveCategory',
} 

export const TWebsitetextActions = {
  assignWebsitetext:        'assignWebsitetext',
} 

export const TPuzzleActions = {
  assignPuzzles:        'assignPuzzles',
} 



export const TLanguageMachineActions = {
  assignLanguagesFromMachine:       'assignLanguagesFromMachine',
  assignLanguagesErrorFromMachine:  'assignLanguagesErrorFromMachine',
} 

export const TGameMachineActions = {
  forwardEvent:   'forwardEvent',
} 

export const TCrosswordLanguageSelectActions = {
  assignActiveSiteLang:         'assignActiveSiteLang',
  assignActiveClueLang:         'assignActiveClueLang',
  assignActiveCrossLang:        'assignActiveCrossLang',
} 

export const TPageActions = {
  selectCrosswordPage:     'selectCrosswordPage',
  selectDictionaryPage:    'selectDictionaryPage',
} 


