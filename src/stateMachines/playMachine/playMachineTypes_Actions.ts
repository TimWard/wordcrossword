//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

export const TPlayActions = {
  squareKeyDown:                'squareKeyDown',
  enterLetter:                  'enterLetter',
  squareUpdate:                 'squareUpdate',
  enterArrow:                   'enterArrow',
  enterTab:                     'enterTab',

  revealThis:                   'revealThis',
  skipThis:                     'skipThis',
  revealAll:                    'revealAll',
  assignActiveSquare:           'assignActiveSquare',
  assignActiveClue:             'assignActiveClue',
  assignInitialActiveClue:      'assignInitialActiveClue',
  assignFromActivePuzzleStates: 'assignFromActivePuzzleStates',

  addPoints:                    'addPoints',
  moveToNextClue:               'moveToNextClue',
  moveToNextSquare:             'moveToNextSquare',
} 


export const TTitleLettersActions = {
  assignTitleLetters:        'assignTitleLetters',
  assignTitleAnimationCss1:  'assignTitleAnimationCss1',
  assignTitleAnimationCss2:  'assignTitleAnimationCss2',
} 
