//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { ActionFunctionMap } from 'xstate'
import { assign, DoneInvokeEvent } from 'xstate'

// fp-ts
import * as O from 'fp-ts/lib/Option'


import { IGameContext } from './gameMachineTypes_Context';
import { IPuzzle, IWord } from 'crosstypes';

import * as gameMachineActions from '../gameMachine/gameMachineTypes_Actions';
import * as gameMachineEvents from '../gameMachine/gameMachine_Events';


export const gameActions: ActionFunctionMap <IGameContext, any> = 
{
  [gameMachineActions.TGridActions.assignGrids]: 
    assign <IGameContext, DoneInvokeEvent <IPuzzle>> 
    ({ activePuzzle: (context, event) => 
        context.activePuzzle = event.data 
    }),

  [gameMachineActions.TGameLanguageSelectActions.assignActiveClueLang]: 
    assign <IGameContext, gameMachineEvents.TSelectClueLangEvent> 
      ({ activeClueLang: (_, event) => event.value }),
  
  // Cluewords
  //------------------------------------------
  [gameMachineActions.TClueWordsActions.assignCluewords]: assign <IGameContext, DoneInvokeEvent <IWord[]>> 
    ({ clueWords: (context, event) => O.some (event.data) }),
     
}

