//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------


// Would be nice to use types!
export const addTests = 
(config: any, tests: any)
: any => {
  return {
    ...config,
    states: Object.entries <{meta: any}> (config.states as any).reduce((state, [stateKey, stateValue]) => {
      return {
        ...state,
        [stateKey]: {
          ...stateValue,
          meta: {
            ...stateValue.meta,
            test: tests[stateKey],
          },
        },
      } 
    }, {}),
  } 
}

