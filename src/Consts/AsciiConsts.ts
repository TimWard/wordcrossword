//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// AsciiConsts
export enum ASCII {
  BACKSPACE = 8,
  DELETE = 46,
  TAB = 9,
  LEFT = 37,
  UP = 38,
  RIGHT = 39,
  DOWN = 40
}
