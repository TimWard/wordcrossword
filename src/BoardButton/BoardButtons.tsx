//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
//import * as cssUtils from '../Utilities/cssUtils';

// TSX sub-components
import {BoardButton} from './BoardButton';

// Interface to the state machine
import * as playEvents from '../stateMachines/playMachine/playMachineTypes_Events';
import { TPlayState } from '../stateMachines/playMachine/playMachine_Options';
import { ISendPlayState } from '../stateMachines/playMachine/playMachineTypes';

export interface IBoardButtonsProps {
  getTranslatedText: (englishText: string) => string
  playState: TPlayState
	sendPlayState: ISendPlayState
  siteLangId: number
}


export const BoardButtons = React.memo (
(props: IBoardButtonsProps) => 

  <div>
    <div>
      {/*<div style = {{color:  cssUtils.getRandomColor ()}}>Test</div>*/}
      <BoardButton 
        props     = {props}
        eventId   = {playEvents.SKIP_THIS}
        getText   = {() => 'Skip this'}
      />
    </div>
    <div>
      <BoardButton 
        props   = {props}
        eventId = {playEvents.REVEAL_THIS}
        getText = {() => 'Reveal this'}
      />
    </div>
    <div>
      <BoardButton 
        props = {props}
        eventId = {props.playState.nextEvents.includes (playEvents.REVEAL_ALL_CONFIRM)
          ? playEvents.REVEAL_ALL_CONFIRM
          : playEvents.REVEAL_ALL}
        getText = { () =>
          props.playState.nextEvents.includes (playEvents.REVEAL_ALL_CONFIRM)
            ? ('Confirm reveal all') 
            : ('Reveal all')
          }
        />
    </div>
  </div>
)

export const BoardButtonSkip320 = (props: {props: IBoardButtonsProps}) => 

  <BoardButton 
    props     = {props.props}
    eventId   = {playEvents.SKIP_THIS}
    getText   = {() => 'Skip'}
  />

export const BoardButtonReveal320 = (props: {props: IBoardButtonsProps}) => 

  <BoardButton 
    props   = {props.props}
    eventId = {playEvents.REVEAL_THIS}
    getText = {() => 'Reveal'}
  />

