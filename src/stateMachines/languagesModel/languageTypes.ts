//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// The top level states, actions and services
//-------------------------------------------

export const TLanguageMachineStates = {
  loading:            'languagemachine_loading',
  crosslang_loader:   'languagemachine_crosslang_loader',
  cluelang_loader:    'languagemachine_cluelang_loader',
  sitelang_loader:    'languagemachine_sitelang_loader',
  loaded:             'languagemachine_loaded',
  loading_error:      'languagemachine_loading_error',
} 

export const TLanguageActions = {
  assignCrossLangs:            'assignCrossLangs',
  assignClueLangs:             'assignClueLangs',
  assignSiteLangs:             'assignSiteLangs',
  assignLangsError:            'assignLangsError',
} 

export const TLanguageServices = {
  crosslang_loading:        'crosslang_loading_svc',
  cluelang_loading:         'cluelang_loading_svc',
  sitelang_loading:         'sitelang_loading_svc',
} 

// The lower level states - no actions or services
//------------------------------------------------

export const TCrossLangStates = {
  loading:        'crosslang_loading_state',
  loaded:         'crosslang_loaded_state',
} 

export const TClueLangStates = {
  loading:        'cluelang_loading_state',
  loaded:         'cluelang_loaded_state',
} 

export const TSiteLangStates = {
  loading:        'sitelang_loading_state',
  loaded:         'sitelang_loaded_state',
} 

