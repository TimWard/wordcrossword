//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------
// This file is pure - no tasks, no IO
//------------------------------------------------

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';
import * as A from 'fp-ts/lib/Array'
import * as O from 'fp-ts/lib/Option'

// fp imports
import * as fp_Square from '../Squares/fp_Square'
import {cf_Blankword} from 'crossfuncs';

// class imports
import { IClue, Square } from "crosstypes";
import { ISquare } from "crosstypes";
import { Word, IWord } from 'crosstypes';


//------------------------------------------------------------------


//------------------------------------------------------------------

const capitalise = 
  (word: string)
  : string =>
  word.charAt(0).toUpperCase() + word.substr(1)

export const displayText =
(clue: IClue) =>
(clueWords: IWord[])
: string =>
  pipe (
    clueWords,
    findClueWord (clue),
    O.fold(
      () => '',
      clueWord => capitalise (clueWord.local)
          + ' ('  + Word.getCrossWordDisplayLength (clue.crossWord) + ')'
    )
  )

const findClueWord =
(clue: IClue) =>
(clueWords: IWord[])
: O.Option <IWord> => 
  A.findFirst <IWord> (word => word.id === clue.crossWord.id) (clueWords)

export const isActiveClue =
  (clue: IClue) =>
  (activeClue: IClue)
  : boolean =>
   (clue.blankword === activeClue.blankword)


// If there are no incorrect square in the clue,
// then return the supplied active square
export const getFirstIncorrectSquareAfterActiveSquare =
(squares: ISquare[][]) =>
(activeSquare: ISquare) =>
(clue: IClue)
: ISquare => 

  pipe (
    cf_Blankword.blankwordSquares (clue.blankword) (squares),
    A.dropLeftWhile (square => square !== activeSquare),
    A.findFirst (square => !Square.isSquareCorrect (square)),
    O.fold(
      () => activeSquare,
      nextSquare => nextSquare
    )
  )

export const getNextSquareByDirection = 
(squares: ISquare[][]) =>
(clue: IClue) =>
(activeSquare: ISquare) =>
(forwards: boolean)
: ISquare =>
  pipe (
    cf_Blankword.blankwordSquares (clue.blankword) (squares),
    squares => forwards ? squares: A.reverse <ISquare> (squares),
    A.dropLeftWhile <ISquare> (square => square !== activeSquare),
    A.dropLeftWhile <ISquare> (square => square === activeSquare),
    A.head,
    O.fold(
      () => activeSquare,
      nextSquare => nextSquare
    )
  )


// This assumes that the activeClue will never be null
export const getClueFromSquare = 
  (activeClue: IClue) => 
  (activeSquare: ISquare) =>
  (square: ISquare)
  : IClue => {

    // If the current clue is not for the selected square,
    // then return the square's priority clue 
    if (!fp_Square.doesSquareMatchClue (square) (activeClue)) 
      return fp_Square.getPriorityClue (square)
    else 
    if (activeSquare === square)
      // User has clicked on the same square twice, 
      // then return the clue in the perpendicular direction
      return fp_Square.getPerpendicularClue (square) (activeClue)
    else
      return activeClue
  }  

