//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { createMachine, MachineOptions } from 'xstate'

// Crossword machine Types
import { ICrosswordContext } from './crosswordTypes_Context'
import { TCrosswordState } from './crosswordTypes'

// Crossword machine implementations
import { crosswordMachineConfig } from './crosswordMachine'
import { crosswordContext } from './crosswordMachine_Context'
import { TCrosswordEventTypes } from './crosswordMachine_Events'
import { crosswordActions } from './crosswordMachine_Actions'
import { crosswordServices } from './crosswordMachine_Services'


const crosswordMachineOptions: Partial<MachineOptions<ICrosswordContext, TCrosswordEventTypes>> =
{
  services: crosswordServices,
  actions: crosswordActions,
}

//export const crosswordMachine = Machine (crosswordMachineConfig)
export const crosswordMachine = 
  createMachine <ICrosswordContext, any, TCrosswordState> 
  (crosswordMachineConfig, crosswordMachineOptions)
  .withContext (crosswordContext)

