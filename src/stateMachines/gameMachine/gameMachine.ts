//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { EventObject, MachineConfig } from 'xstate'

// My machine types
import { TMachineIds, TInvokedMachineIds } from '../crosswordMachineIds'
import { IGameContext } from './gameMachineTypes_Context';


// Imports from the game model
import * as gameMachineActions from './gameMachineTypes_Actions';
import * as gameMachineServices from './gameMachineTypes_Services';
import * as gameMachineStates from './gameMachineTypes_States';

import * as languageEvents from '../languageTypes_Events';
import { playMachine } from '../playMachine/playMachine_Options';

import * as fp_gamePure from '../../Game/fp_gamePure';

type TGameMachineConfig = MachineConfig <IGameContext, any, EventObject>

export const gameMachineConfig: TGameMachineConfig = {
  id: TMachineIds.gameMachine,
  initial: gameMachineStates.TGridStates.loadGrids,
  states: {
    [gameMachineStates.TGridStates.loadGrids]: {
      initial: gameMachineStates.TGridStates.grids_loading,
      states: {
        [gameMachineStates.TGridStates.grids_loading]: {
          invoke: {
            id: 'grids_loading_Id',
            src: gameMachineServices.TGridServices.grids_loading_svc,
            onDone: {
              target: gameMachineStates.TGridStates.grids_loaded,
              actions: [gameMachineActions.TGridActions.assignGrids]
            },
            onError: {
              target: gameMachineStates.TGridStates.grids_loading_error,
//                      actions: TWebsitetextActions.assignCategoriesError
            }
          }
        },
        [gameMachineStates.TGridStates.grids_loaded]: {
          initial: gameMachineStates.TGameStates.selectClueLang,
          states: {
            [gameMachineStates.TGameStates.selectClueLang]: {
              initial: gameMachineStates.TGameStates.loadClueWords,
              states: {
                [gameMachineStates.TGameStates.loadClueWords]: {
                  initial: gameMachineStates.TClueWordsStates.cluewords_loading,
                  states: {
                    [gameMachineStates.TClueWordsStates.cluewords_loading]: {
                      invoke: {
                        id: 'cluewords_loading_Id',
                        src: gameMachineServices.TClueWordsServices.cluewords_loading_svc,
                        onDone: {
                          target: gameMachineStates.TClueWordsStates.cluewords_loaded,
                          actions: gameMachineActions.TClueWordsActions.assignCluewords
                        },
                        onError: {
                          target: gameMachineStates.TClueWordsStates.cluewords_loading_error,
    //                      actions: TWebsitetextActions.assignCategoriesError
                        }
                      }
                    },
                    [gameMachineStates.TClueWordsStates.cluewords_loaded]: {
                      initial: gameMachineStates.TPlayGameMachineStates.invoke_play_machine,
                      states: {
                        [gameMachineStates.TPlayGameMachineStates.invoke_play_machine]: {
                          invoke: {
                            id: TInvokedMachineIds.playMachine,
                            src: playMachine,
                            autoForward: true, // Todo - this is not recommended!
                            data: {
                              // This should match the structure of IPlayContext
                              // Note that we don't specify the context type, because xState uses is incorrectly
                              // (It uses it for the type of both the source and the dest machine contexts!)
                              // Note that we have to specify the entire context
                              // this is due to a bug in xState which should be fixed in xState 5.0
                              activeClueLang: (context: any, event: any) => context.activeClueLang,
                              activeCrossLang: (context: any, event: any) => context.activeCrossLang,
                              activeCategory: (context: any, event: any) => context.activeCategory,
                              clueWords: (context: any, event: any) => context.clueWords,                              
                              gameData: (context: any, event: any) => context.gameData,

                              activePuzzle: (context: any, event: any) => context.activePuzzle,
                              activeClue: (context: any, event: any) => context.activePuzzle.clues[0],
                              activeSquare: (context: any, event: any) => 
                                fp_gamePure.getActiveSquareFromActiveClue (context.activePuzzle.grid.squares) (context.activePuzzle.clues[0]),

                              titleLetters: (context: any, event: any) => 'WORDCROSSWORD',
                              titleAnimationCss: (context: any, event: any) => '',                              
                              points: (context: any, event: any) => 0,                              
                            }
                          }
                        },
                      },
                    },
                    [gameMachineStates.TClueWordsStates.cluewords_loading_error]: {
                      type: 'final'
                    }
                  },
                }
              },
              on: {
                [languageEvents.TLanguageEvents.SELECT_CLUELANG]: {
                  target:   gameMachineStates.TGameStates.selectClueLang,
                  actions:  gameMachineActions.TGameLanguageSelectActions.assignActiveClueLang,
                }
              }
            },
          },
        },
        [gameMachineStates.TGridStates.grids_loading_error]: {
          type: 'final'
        }
      },
    },
  }
}  

