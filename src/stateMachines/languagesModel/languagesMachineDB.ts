//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import { ILanguage } from "crosstypes";
import { ILanguageData } from 'dblib';


// Todo - loadLanguagesFromDbByDay should return a TaskEither
export const loadCrossLangs = 
(languageData: ILanguageData) =>
(siteLangId: number) =>
(dayIndex: number):
Promise <ILanguage[]> =>
//  Promise.reject('my Error')  // Don't delete, this is used for dev testing
  languageData.loadLanguagesFromDbByDay (dayIndex) (siteLangId) ()

//   .then (
//   E.fold (
//     err => {throw new Error (err.message)},
//     categories => categories
//   )
// )

export const loadClueOrSiteLangs = 
(languageData: ILanguageData) =>
(siteLangId: number):
Promise <ILanguage[]> =>
//  () => createDefaultLanguage ()
//  Promise.reject('my Error')  // Don't delete, this is used for dev testing
  languageData.loadLanguagesFromDbById (siteLangId) ()

