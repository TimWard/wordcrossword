//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';

import { ILanguage, TLanguagePair } from "crosstypes";

// fp imports
import * as fp_Language from '../Languages/fp_Language'

export const FLAGCODES = {
  ENGLISH: 'GB',
  FRENCH: 'FR',
  SPANISH: 'ES'
};


export const findLangById =
(languages: ILanguage[]) => 
(suppliedLanguage: ILanguage): 
ILanguage => 
  languages.find (language => language.id === suppliedLanguage.id)
  || fp_Language.createDefaultLanguage ()

export const getLangFromCode =
(languages: ILanguage[]) => 
(code: string):
ILanguage => 
  languages.find (language => language.code === code)
  || fp_Language.createDefaultLanguage ()
    
export const findFromFlag =
(flag: string) => 
(languages: ILanguage[]):
ILanguage => 
  languages.find (language => language.flag === flag)
  || fp_Language.createDefaultLanguage ()
    
  

export const getLangFromFlag =
(languages: ILanguage[]) => 
(flag: string):
ILanguage => 
  languages.find (language => language.flag === flag)
  || fp_Language.createDefaultLanguage ()

// English will be the default clue language, 
// Rule Britannia!
export const getDefaultClueLang =
(languages: ILanguage[]):
ILanguage => 
  getLangFromFlag (languages) (FLAGCODES.ENGLISH)

export const asCodes =
(languages: ILanguage[]):
string[] => 
  languages.map (language => language.flag)

  
export const asPairs =
(languages: ILanguage[]):
TLanguagePair[] => 
  languages.map (language => 
     ({key: language.flag, value: language.name} as TLanguagePair)
  )
  

// This will only load languages that have a puzzle for the current day
// It will return the languages translated into the supplied language
// English will be the default clue language! Rule Britannia!
// (Unless it is already the cross language!)
// export const loadFromByDayDB =
// (languageData: ILanguageData) =>
// (siteLangId: number, dayIndex: number)
// : T.Task <ILanguage[]> => 
//   languageData.loadLanguagesFromDbByDay (dayIndex) (siteLangId)



// // This will load ALL the languages
// // It will return the languages translated into the supplied language
// export const loadFromDBBySourceLang =
// (languageData: ILanguageData) =>
// (siteLangId: number)
// : T.Task <ILanguage[]> =>

//   languageData.loadLanguagesFromDbById (siteLangId)



export const doOnSelect = 
(languages: ILanguage[]) => 
(onSelect: (language: ILanguage) => void) => 
(flag: string)
: ILanguage =>
  pipe (
    findFromFlag (flag) (languages),
    language => pipe (
      onSelect (language),
      () => language,
    )
  )
