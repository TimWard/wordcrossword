//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

export const TGridServices = {
  grids_loading_svc:       'grids_loading_svc',
} 

export const TClueWordsServices = {
  cluewords_loading_svc:       'cluewords_loading_svc',
} 

