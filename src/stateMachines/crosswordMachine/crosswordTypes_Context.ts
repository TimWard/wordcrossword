//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// Crossword types
import { ICategory, ILanguage, IWebsitetext, IPuzzle } from 'crosstypes';
import { IGameData } from 'dblib';


export interface ICrosswordContext {
  dayIndex: number

  browserLang: string
  screenWidth: number,

  // Loaded and passed to the game machine
  activePuzzle: IPuzzle

  gameData: IGameData

  categories: ICategory[]
  activeCategory: ICategory

  crossLangs: ILanguage[]
  activeCrossLang: ILanguage

  clueLangs: ILanguage[]
  activeClueLang: ILanguage

  siteLangs: ILanguage[]
  activeSiteLang: ILanguage

  websitetexts: IWebsitetext[]
}
