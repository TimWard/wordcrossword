//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// Crossword types
import { ICategory } from 'crosstypes';
import { ILanguage } from 'crosstypes';

import * as crosswordEvents from './crosswordTypes_Events';
import * as languageEvents from '../languageTypes_Events';


export type TSelectCategoryEvent = 
{
  type: typeof crosswordEvents.SELECT_CATEGORY
  value: ICategory
}

export type TSelectCrossLangEvent = 
{
  type: typeof languageEvents.SELECT_CROSSLANG
  value: ILanguage
}

export type TSelectSiteLangEvent = 
{
  type: typeof languageEvents.SELECT_SITELANG
  value: ILanguage
}

// Note that this event is not used here
// it is only included to make the compiler happy
// because we use it in the generic languageDropdown.tsx
export type TSelectClueLangEvent = 
{
  type: typeof languageEvents.SELECT_CLUELANG
  value: ILanguage
}

export type TCrosswordEventTypes = 
  TSelectCategoryEvent | 
  TSelectCrossLangEvent | 
  TSelectSiteLangEvent |
  TSelectClueLangEvent

