//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//-----------------------------------------------------------------
// This file should contain as much dirt as possible
//-----------------------------------------------------------------

import * as React from 'react';
import * as ReactDOM from 'react-dom';

// fp imports
import * as fp_Screen from './Screen/fp_Screen';

import {Game} from './Game/Game';
import { gameData } from 'dblib';
// import { gameTestData } from './Game/GameTestData';
//import {TestGame} from './Game/TestGame.jsx';


import {setServerPort, setBuildTarget, DB_SERVER_PORTS} from 'dblib';
import { buildConfig } from './myConfig';

setServerPort (DB_SERVER_PORTS.TEMPTEST);
setBuildTarget (buildConfig.target)

ReactDOM.render (
  <Game
    // All IO should be pushed to the outer limits of the app
    isScreenWidth768 = {fp_Screen.isScreenWidth768()}
//    gameData = {gameTestData}
    gameData = {gameData}
  />, 
  document.getElementById ('content')
)
//ReactDOM.render (<TestGame/>, document.getElementById ('content'))

  

