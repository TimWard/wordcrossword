//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// Game events
// ------------------------------------------

export const SELECT_CLUE      = 'SELECT_CLUE'
export const SELECT_SQUARE    = 'SELECT_SQUARE'
export const ENTER_CHAR       = 'ENTER_CHAR'
export const ENTER_LETTER     = 'ENTER_LETTER'
export const SQUARE_UPDATE    = 'SQUARE_UPDATE'
export const ENTER_ARROW      = 'ENTER_ARROW'
export const ENTER_TAB        = 'ENTER_TAB'

export const REVEAL_THIS      = 'REVEAL_THIS'
export const SKIP_THIS        = 'SKIP_THIS'

export const REVEAL_ALL           = 'REVEAL_ALL'
export const REVEAL_ALL_CONFIRM   = 'REVEAL_ALL_CONFIRM'


export const TPlayEvents = {
  SELECT_CLUE:          SELECT_CLUE,
  ENTER_CHAR:           ENTER_CHAR,
  ENTER_LETTER:         ENTER_LETTER,
  SQUARE_UPDATE:        SQUARE_UPDATE,
  ENTER_ARROW:          ENTER_ARROW,
  ENTER_TAB:            ENTER_TAB,

  REVEAL_THIS:          REVEAL_THIS,
  SKIP_THIS:            SKIP_THIS,

  REVEAL_ALL:           REVEAL_ALL,
  REVEAL_ALL_CONFIRM:   REVEAL_ALL_CONFIRM,
} 

