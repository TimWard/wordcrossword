//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import * as T from 'fp-ts/lib/Task'

import { IWebsitetextData } from 'dblib';
import { IWebsitetext } from 'crosstypes';


export const loadWebsitetextFromDB = 
(languageId: number)
: T.Task <IWebsitetext[]> => 
T.of ([{
  id: 400,
  englishtext: 'Test Cat1 Name',
  text: 'Test Translation - Test Cat1',
}])

export const websitetextTestData: IWebsitetextData = {
  loadWebsitetextFromDB,
}
