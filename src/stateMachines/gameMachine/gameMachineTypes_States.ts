//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

export const TClueWordsStates = {
  cluewords_loading:       'cluewords_loading',
  cluewords_loaded:        'cluewords_loaded',
  cluewords_loading_error: 'cluewords_loading_error',
} 

export const TGridStates = {
  loadGrids:            'loadGrids',
  grids_loading:        'grids_loading',
  grids_loaded:         'grids_loaded',
  grids_loading_error:  'grids_loading_error',
} 

export const TGameStates = {
  loadClueWords:    'loadClueWords',
  selectClueLang:   'selectClueLang',
} 

export const TPlayGameMachineStates = {
  invoke_play_machine:     'invoke_play_machine',
}




