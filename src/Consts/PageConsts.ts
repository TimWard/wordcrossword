//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

export const PAGE_CONSTS = {
	CROSSWORDS_TEXT:  'Crosswords',
	DICTIONARY_TEXT:  'Dictionary',
	FEEDBACK_TEXT:    'Feedback',
};

