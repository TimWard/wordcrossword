//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { assign, DoneInvokeEvent, ActionFunctionMap, ErrorExecutionEvent } from 'xstate'

// My types
import { ICategory, IWebsitetext, IPuzzle, ILanguage } from 'crosstypes';

// My functions
import * as fp_Category from '../../Categories/fp_Category'

// Crossword machine Types
import { ICrosswordContext } from './crosswordTypes_Context'
import * as crosswordMachineActions from './crosswordTypes_Actions'

// Crossword machine implementations
import * as crosswordMachineEvents from './crosswordMachine_Events'


type languageMachineDoneEvent = {
  crossLangs:  ILanguage[]
  clueLangs:  ILanguage[]
  siteLangs:  ILanguage[]
}


export const crosswordActions: ActionFunctionMap <ICrosswordContext, any> = 
{
  // Categories
  //------------------------------------------

  [crosswordMachineActions.TCategoriesActions.assignCategories]: assign <ICrosswordContext, DoneInvokeEvent <ICategory[]>> 
    ({ categories: (context, event) => event.data }),
  // Todo - replace DoneInvokeEvent with ErrorExecutionEvent
  [crosswordMachineActions.TCategoriesActions.assignCategoriesError]: assign <ICrosswordContext, DoneInvokeEvent <string>>
    ({ categories: (context, event) => [fp_Category.createErrorCategory (event.data)] }),
  [crosswordMachineActions.TCategoriesActions.assignActiveCategory]: assign <ICrosswordContext, crosswordMachineEvents.TSelectCategoryEvent> 
    ({activeCategory: (_, event) => event.value}),

  // Websitetext
  //------------------------------------------
  [crosswordMachineActions.TWebsitetextActions.assignWebsitetext]: assign <ICrosswordContext, DoneInvokeEvent <IWebsitetext[]>> 
    ({ websitetexts: (context, event) => event.data }),


  // Languages
  //------------------------------------------
  [crosswordMachineActions.TLanguageMachineActions.assignLanguagesFromMachine]: assign <ICrosswordContext, DoneInvokeEvent <languageMachineDoneEvent>>  
    ({
      crossLangs: (context, event) => event.data.crossLangs,
      clueLangs: (context, event) => event.data.clueLangs,
      siteLangs: (context, event) => event.data.siteLangs,
    }),

  [crosswordMachineActions.TLanguageMachineActions.assignLanguagesErrorFromMachine]: assign <ICrosswordContext, ErrorExecutionEvent>  
    ({}), // ({crossLangs: (context, event) => event.data.languages}),

  
  [crosswordMachineActions.TCrosswordLanguageSelectActions.assignActiveCrossLang]: assign <ICrosswordContext, crosswordMachineEvents.TSelectCrossLangEvent> 
    ({ activeCrossLang: (_, event) => event.value }),
          
  [crosswordMachineActions.TCrosswordLanguageSelectActions.assignActiveSiteLang]: assign <ICrosswordContext, crosswordMachineEvents.TSelectSiteLangEvent> 
    ({ activeSiteLang: (_, event) => event.value }),

  [crosswordMachineActions.TCrosswordLanguageSelectActions.assignActiveClueLang]: assign <ICrosswordContext, crosswordMachineEvents.TSelectClueLangEvent> 
    ({ activeClueLang: (_, event) => event.value }),

  // Puzzles
  //------------------------------------------
  [crosswordMachineActions.TPuzzleActions.assignPuzzles]: assign <ICrosswordContext, DoneInvokeEvent <IPuzzle[]>> 
    ({ activePuzzle: (context, event) => {
      // console.log ('assignPuzzles: activePuzzle = ')
      // console.log (event.data[0])
    
      return event.data[0]}
     }),

}

