//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';

// fp-ts imports
import * as O from 'fp-ts/lib/Option'
import { pipe } from 'fp-ts/lib/pipeable';

// Interface imports
import { IClue } from 'crosstypes';

// Pure fp imports
import * as fp_Clue from './fp_Clue';
import { IWord } from 'crosstypes';

interface ISingleClueProps {
  activeClue: IClue
  clueWords: O.Option <IWord[]>
}

export const SingleClue = (props: ISingleClueProps) => 
  
    <button className = 'singleClue'>
      {
        pipe (
          props.clueWords,
          O.fold (
            () => undefined,
            fp_Clue.displayText (props.activeClue) 
          )
        )
      }
    </button>

