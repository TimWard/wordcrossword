//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';

// fp-ts imports
import * as T from 'fp-ts/lib/Task'
import { pipe } from 'fp-ts/lib/pipeable';
import * as A from 'fp-ts/lib/Array'
import * as O from 'fp-ts/lib/Option'

// Interface imports
import {WebsitetextElement} from '../Websitetext/Websitetext';
import {DICTIONARYCONSTS} from './DictionaryConsts';
import { ICategory, ILanguage, createEmptyWord } from 'crosstypes';
import { IWordData } from 'dblib';


// My libs
import { IWord } from 'crosstypes';

// pure functions
import * as fp_Category from '../Categories/fp_Category';


const WORD_LENGTH_FILTERS = [
	{title: 'Three Letters', filter: 3},
	{title: 'Four Letters', filter: 4},
	{title: 'Five Letters', filter: 5},
	{title: 'Six Letters', filter: 6},
	{title: 'Seven Letters', filter: 7},
	{title: 'Eight Letters', filter: 8},
	{title: 'Nine Letters', filter: 9},
	{title: 'Ten Letters', filter: 10},
	{title: 'Eleven Letters', filter: 11},
]

interface IDictionaryProps {
  activeCategory: ICategory
  activeClueLang: ILanguage
  activeCrossLang: ILanguage
  wordData: IWordData
  getTranslatedText: (englishText: string) => string
}

class TWordPair {
  word!: IWord
  foreignWord!: IWord
  constructor (word: IWord, foreignWord: IWord) {
    this.word = word
    this.foreignWord = foreignWord
  }
}

export const Dictionary = 
(props: IDictionaryProps) => {

  const [activeDicSort, setActiveDicSort] = React.useState (DICTIONARYCONSTS.CLUE)
  const [activeWordFilter, setActiveWordFilter] = React.useState ('')
  
  const [clueWords, setClueWords] = React.useState([createEmptyWord()])
  const [crossWords, setCrossWords] = React.useState([createEmptyWord()])

  React.useEffect (() => 
   {pipe (
      props.wordData.loadWordsByLanguageFromDB (props.activeCrossLang.id),
      T.map (setCrossWords)
    ) ()}
  , [props.activeCrossLang.id, props.wordData]) 

  React.useEffect (() => 
    {pipe (
      props.wordData.loadWordsByLanguageFromDB (props.activeClueLang.id),
      T.map (setClueWords)
    ) ()}
  , [props.activeClueLang.id, props.wordData]) 
  

  const getButtonStyle = (name: string) => 
  	activeDicSort === name
			? "button roundedbutton dicfilterbutton green"
			: "button roundedbutton dicfilterbutton blue"

  const getWordFilterButtonStyle = (name: string) => 
  	activeWordFilter === name
			? "button roundedbutton dicfilterbutton green"
			: "button roundedbutton dicfilterbutton blue"

	const renderWordFilterButtons = () => {
		const filterButtons = WORD_LENGTH_FILTERS.map ( (rec) => {
      return (
  			<button className = {getWordFilterButtonStyle (rec.filter.toString())}
          onClick = {(id) => {setActiveWordFilter (rec.filter.toString())}}
        >
          <WebsitetextElement 
            getTranslatedText = {props.getTranslatedText} 
            text = {rec.title}
          />
        </button>
      )
    });

    return (
    	<div>
  			<button className = "button roundedbutton dicfilterbutton gray">
          <WebsitetextElement 
            getTranslatedText = {props.getTranslatedText} 
            text = "Filter by Word Length"
          />
        </button>
  			<button className = "button roundedbutton dicfilterbutton blue"
          onClick = {(id) => {setActiveWordFilter ('')}}
        >
          <WebsitetextElement 
            getTranslatedText = {props.getTranslatedText} 
            text = "Reset"
          />
        </button>
        {filterButtons}
    	</div>
    );
  }

  const renderHeaderButtons = () => {
    return (
    	<div>
  			<button className = {getButtonStyle (DICTIONARYCONSTS.CLUE)}
          onClick = {(id) => {setActiveDicSort (DICTIONARYCONSTS.CLUE)}}
        >
          {props.activeClueLang.name}
        </button>
	      <button className = {getButtonStyle (DICTIONARYCONSTS.CROSS)}
          onClick = {() => {setActiveDicSort (DICTIONARYCONSTS.CROSS)}}
        >
          {props.activeCrossLang.name}
        </button>
    	</div>
    );
  }


  const filterCrossWords =
  (words: IWord[], activeCategory: ICategory): IWord[] => {
  	const rank = fp_Category.getRank (activeCategory)
  	const maxWordLength = fp_Category.getMaxWordLength (activeCategory)
    const filterLength = activeWordFilter

  	return words.filter ((word) =>
            ((word.crossText.length > 2) 
      		&& (word.crossText.length <= maxWordLength) 
      		&& (word.id < rank)
      		&& (filterLength === '' ||
      		    filterLength === word.crossText.length.toString() ))
    )
    
  }

  // Takes 2 lists of words and creates a single list of word pairs
  // Todo - use fp-ts zip, after sorting words by id
  const combineWordLists =
  (clueWords: IWord[]) => 
  (crossWords: IWord[])
  : TWordPair[] => 

    crossWords.map ((crossWord) => 
      pipe (
        A.findFirst <IWord> (clueWord => crossWord.id === clueWord.id) (clueWords),
        O.fold(
          () => new TWordPair (createEmptyWord(), createEmptyWord() ), // Should never get called
          clueWord =>  new TWordPair (clueWord, crossWord)
        )
      )
    )
  

  const renderWords =
  (clueWords: IWord[], crossWords: IWord[], activeCategory: ICategory) => {

    var wordsCombined = pipe (
    	filterCrossWords (crossWords, activeCategory),
      combineWordLists (clueWords) 
    )

    // Can't use the fp-ts sort here, because we need to use localeCompare
    // const ordByWord = Ord.ord.contramap(Ord.ordString, ((wp: TWordPair) => wp.word.local))
    // const ordByForeignWord = Ord.ord.contramap(Ord.ordString, ((wp: TWordPair) => wp.foreignWord.local))

    activeDicSort === DICTIONARYCONSTS.CLUE
      // ? A.sortBy([ordByWord]) (wordsCombined)
      // : A.sortBy([ordByForeignWord]) (wordsCombined)
	  	? wordsCombined.sort ( (a: any, b: any) => a.word.local.localeCompare(b.word.local))
  	  : wordsCombined.sort ( (a: any, b: any) => a.foreignWord.local.localeCompare(b.foreignWord.local))

    return wordsCombined.map ( (wordPair: any, move: number) => {
      return (
        <li key = {move}>
     			<button className = "button roundedbutton dicfilterbutton gray"
//            border: category.borderColor (props.game.activeCategory)}}
//              onClick = {(id) => category.handleCategoryClick ()}
          >
          	{wordPair.word.local}
          </button>
     			<button className = "button roundedbutton dicfilterbutton gray"
//            border: category.borderColor (props.game.activeCategory)}}
//              onClick = {(id) => category.handleCategoryClick ()}
          >
          	{wordPair.foreignWord.local}
          </button>
        </li>
      );
    });
  }

  return (
    <div className="dictionary-wrapper">
      <div className="dictionary">
        {renderHeaderButtons()}
        <ol>
          {renderWords (clueWords, crossWords, props.activeCategory)}
        </ol>
      </div>
      <div className="dictionary-options">
        {renderWordFilterButtons()}
      </div>
    </div>
  )

}
