//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

export const TPlayCrosswordGuards = {
  isCrosswordComplete:         'isCrosswordComplete',
  isCrosswordIncomplete:       'isCrosswordIncomplete',
  isCrosswordClean:            'isCrosswordClean',
  isCrosswordDirty:            'isCrosswordDirty',
} 

export const TPlayClueGuards = {
  isClueComplete:             'isClueComplete',
  isClueIncomplete:       'isClueIncomplete',
  isClueClean:            'isClueClean',
  isClueDirty:            'isClueDirty',
} 

export const TPlaySquareGuards = {
  isSquareCorrect:         'isSquareCorrect',
  isSquareIncorrect:       'isSquareIncorrect',
} 


