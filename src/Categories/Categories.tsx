//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
//import * as cssUtils from '../Utilities/cssUtils';

// Interfaces
import { ICategory } from 'crosstypes';

// Interface to the state machine
import * as crosswordEvents from '../stateMachines/crosswordMachine/crosswordTypes_Events';
import { ISendCrosswordMachineState } from '../Game/GameStateInterface';
import { IWebsitetextElement } from '../Websitetext/WebsitetextTypes';

interface ICategoriesProps {
  getTranslatedText: (englishText: string) => string
  sendMachineState: ISendCrosswordMachineState
  activeCategoryId: number
  categories: ICategory[]
  activeCrossLangId: number // For testing only
  WebsitetextElement: IWebsitetextElement
}


const onCategoryClick = 
(props: ICategoriesProps) => 
(category: ICategory) =>
  category.id !== props.activeCategoryId 
    ? props.sendMachineState ({type: crosswordEvents.SELECT_CATEGORY, value: category})
    : undefined
    

    
const renderCategory = 
(props: ICategoriesProps) => 
(category: ICategory) =>
  <li key = {category.id}>
    <button 
      className = {categoryButtonCssClass (category.id) (props.activeCategoryId)} 
      onClick = {() => onCategoryClick (props) (category)}
    >
      <props.WebsitetextElement 
        getTranslatedText = {props.getTranslatedText} 
        text = {category.name}
      />
    </button>
  </li>

export const CategoriesElement = React.memo (
//export const Categories =  (
//  <div style = {{color:  cssUtils.getRandomColor ()}}>Active CrossLang: {props.activeCrossLangId}</div>
  (props: ICategoriesProps) => 

  <div className="categories">
    <div className="categoriesHeader">
      <props.WebsitetextElement 
        getTranslatedText = {props.getTranslatedText} 
        text = "Difficulty"
      />
    </div>
    <ol>
        {props.categories.map (renderCategory (props))}
    </ol>
  </div>
)

const categoryButtonCssClass = 
(categoryId: number) =>
(activeCategoryId: number)
: string => 
  categoryId === activeCategoryId
  ? 'button roundedbutton green categoryButton'
  : 'button roundedbutton blue categoryButton'
