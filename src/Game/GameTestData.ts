//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import { IGameData, clueData, wordData, languageData, gridData, squareData, dailyPuzzleData, feedbackData } from "dblib";

// Test data - todo - move this to its own file
import { categoryTestData }  from "../Categories/CategoryTestData";
import { websitetextTestData } from "../Websitetext/WebsitetextTestData";

export const gameTestData: IGameData = {
  dailyPuzzleData,
  clueData: clueData,
  languageData,
  wordData,
  gridData,
  squareData,
  categoryData: categoryTestData,
  websitetextData: websitetextTestData,
  feedbackData: feedbackData

}

