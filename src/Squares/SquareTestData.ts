//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import * as T from 'fp-ts/lib/Task'

import { ISquareData } from "dblib";
import { ISquare } from 'crosstypes';

const loadSquaresFromDB =
(gridId: number) 
: T.Task <ISquare[][]> => 
  T.of (
    [
      [
        {gridId: 1, row: 1, col: 1, isBlack: false, answer: '', userLetter: ''},
      ]
    ]
  )

export const squareTestData: ISquareData = {
  loadSquaresFromDB,
}
