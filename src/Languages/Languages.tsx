//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';

// fp imports
import * as fp_Languages from './fp_Languages';

// Interface imports
import { ILanguage } from 'crosstypes';
import { ILanguageData } from 'dblib';


// Interface to the state machine
import { ISendCrosswordMachineState } from '../Game/GameStateInterface';
import { ISendGameState } from '../stateMachines/gameMachine/gameMachineTypes';
import { LanguageDrowdown } from './LanguageDropdown';

// Interface to the state machine
import * as languageEvents from '../stateMachines/languageTypes_Events';

interface ILanguagesProps {
  // Common lang props
  isScreenWidth768: boolean
  getTranslatedText: (englishText: string) => string

  // xState interface
	sendMachineState: ISendCrosswordMachineState
  sendGameState: ISendGameState
  
  // Site lang props
  siteLangs: ILanguage[]
  browserLang: string

  // Cross lang props
  crossLangs: ILanguage[]
  activeCrossLang: ILanguage;
          
  // Clue lang props
  clueLangs: ILanguage[]
  activeClueLang: ILanguage;

  // data access
  languageData: ILanguageData

  // These properties may not be used directly,
  // but we need to refresh when the site language changes
  activeSiteLang: ILanguage;
}


export const Languages = React.memo (
(props: ILanguagesProps) => {

  const render = () => 
  
    <div className="languages">
      {renderSiteLanguage ()}
      {renderClueLanguage ()}
      {renderCrossLanguage ()}
    </div>
  
  
  const renderSiteLanguage = () => 

    <LanguageDrowdown 
      getTranslatedText = {props.getTranslatedText} 
      title             = "Language of Site"
      sendMachineState  = {props.sendMachineState}
      languages         = {props.siteLangs}         
      defaultLangFlag   = {getDefaultSiteLangFlag (props.siteLangs) (props.browserLang)}
      eventName         = {languageEvents.SELECT_SITELANG}
    />

  // English will be the default site language! Rule Britannia!
  const getDefaultSiteLangFlag =
    (languages: ILanguage[]) => 
    (browserLang: string):
    string => 

    pipe (
      browserLang.length > 1
        ? browserLang.slice (0,2).toUpperCase()
        : 'EN',
      fp_Languages.getLangFromCode (languages)
    ).flag
    

  const renderClueLanguage = () =>

    <LanguageDrowdown
      getTranslatedText = {props.getTranslatedText} 
      title             = "Language of Clues"
      sendMachineState  = {props.sendMachineState}
      languages         = {props.clueLangs}         
      defaultLangFlag   = {props.activeClueLang.flag}
      eventName         = {languageEvents.SELECT_CLUELANG}    
  />

  const renderCrossLanguage = () => 

    <LanguageDrowdown 
      getTranslatedText = {props.getTranslatedText} 
      title             = "Language of Answers"
      sendMachineState  = {props.sendMachineState}
      languages         = {props.crossLangs}         
      defaultLangFlag   = {props.activeCrossLang.flag}
      eventName         = {languageEvents.SELECT_CROSSLANG}    
    />



    return render ()

}
)
