Warning
-------
1/ This project will not compile because it relies on some libraries that are not publicly available


Information
-----------
1/ This project contains some of the client code for the following website:
  www.wordcrossword.com

2/ The wordcrossword website also relies on the following projects which are not publicly available:
	- A crossword grid generator
	- A crossword puzzle generator
	- A translation app that translates the words into over 80 languages using the Google or Yandex translation APIs
	- An express project that serves up data from a postgres database
	- 3 utility libraries for sharing code between the above projects

Screen sizes
------------
1/ This project has been tested to run on the following screen sizes
	- laptop screen - 1360 x 768
	- mobile screen	- 360 x 640


State Machines
--------------
1/ This project uses xState for the state machines
2/ The stateMachines directory contains the 4 state machines 
3/ These state machines can be seen in action by performing the following steps
	- Add the xState Dev Toools extension to Chrome
	- Go to www.wordcrossword.com
	- Open Chrome's dev tools and select the XState DevTools tab
	- The 4 state machines will be available in the dropdown at the top of the page
		- The state machine called "playMachine" is the most active state machine and controls the game play
