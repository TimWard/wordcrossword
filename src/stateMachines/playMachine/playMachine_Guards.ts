//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------



// xstate
import { ConditionPredicate} from 'xstate'

import { IPlayContext } from './playMachineTypes_Context';

import * as playMachineGuards from './playMachineTypes_Guards';

// Pure fp imports
import * as fp_Squares from '../../Squares/fp_Squares';
import {cf_Blankword} from 'crossfuncs';
import { Square } from 'crosstypes';

export const playGuards: Record <string, ConditionPredicate <IPlayContext, any>> = 
{
  // Crossword guards
  [playMachineGuards.TPlayCrosswordGuards.isCrosswordComplete]: (context, _) => 
    fp_Squares.isCrosswordCorrect (context.activePuzzle.grid.squares),

  [playMachineGuards.TPlayCrosswordGuards.isCrosswordIncomplete]: (context, _) => 
    !fp_Squares.isCrosswordCorrect (context.activePuzzle.grid.squares),


  [playMachineGuards.TPlayCrosswordGuards.isCrosswordClean]: (context, _) => 
    fp_Squares.isCrosswordClean (context.activePuzzle.grid.squares),

  [playMachineGuards.TPlayCrosswordGuards.isCrosswordDirty]: (context, _) => 
    !fp_Squares.isCrosswordClean (context.activePuzzle.grid.squares),


  // Clue guards
  [playMachineGuards.TPlayClueGuards.isClueComplete]: (context, _) => 
    cf_Blankword.isCorrect 
      (context.activePuzzle.grid.squares) (context.activeClue.blankword),

  [playMachineGuards.TPlayClueGuards.isClueIncomplete]: (context, _) => 
    !cf_Blankword.isCorrect 
      (context.activePuzzle.grid.squares) (context.activeClue.blankword),


  [playMachineGuards.TPlayClueGuards.isClueClean]: (context, _) => 
    cf_Blankword.isClean 
      (context.activePuzzle.grid.squares) (context.activeClue.blankword),

  [playMachineGuards.TPlayClueGuards.isClueDirty]: (context, _) => 
    !cf_Blankword.isClean 
      (context.activePuzzle.grid.squares) (context.activeClue.blankword),

  // Square guards
  [playMachineGuards.TPlaySquareGuards.isSquareCorrect]: (context, _) => 
    Square.isSquareCorrect (context.activeSquare),
  
  [playMachineGuards.TPlaySquareGuards.isSquareIncorrect]: (context, _) => 
    !Square.isSquareCorrect (context.activeSquare),
  
        
}


