//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//-------------------------------------------------------------------------
// Machine Ids
//-------------------------------------------------------------------------
// These are all kept together, because they should be unique
//-------------------------------------------------------------------------


export const TMachineIds = {
  crosswordMachine:   'crosswordMachine_Id',
  loadingMachine:     'loadingMachine_Id',
  gameMachine:        'gameMachine_Id',
  playMachine:        'playMachine_Id',
}

export const TInvokedMachineIds = {
  crosswordMachine:   'crosswordMachine_invoked_Id',
  loadingMachine:     'loadingMachine_invoked_Id',
  gameMachine:        'gameMachine_invoked_Id',
  playMachine:        'playMachine_invoked_Id',
}

