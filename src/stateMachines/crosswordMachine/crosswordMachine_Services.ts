//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { ServiceConfig } from 'xstate'

import * as crosswordMachineDB from './crosswordMachineDB';

// Crossword machine Types
import { ICrosswordContext } from './crosswordTypes_Context'
import * as crosswordMachineServices from './crosswordTypes_Services'


export const crosswordServices: Record <string, ServiceConfig <ICrosswordContext>> = 
{
  [crosswordMachineServices.TCategoriesServices.categories_loading_svc]: (context, _) => 
    crosswordMachineDB.loadCategories 
      (context.gameData) 
      (context.activeCrossLang.id) 
      (context.dayIndex),

  [crosswordMachineServices.TWebsitetextServices.websitetext_loading_svc]: (context, _) => 
    crosswordMachineDB.loadWebsitetext 
      (context.gameData) 
      (context.activeSiteLang.id),

  [crosswordMachineServices.TPuzzleServices.puzzles_loading_svc]: (context, _) => 
    crosswordMachineDB.loadPuzzles 
      (context.gameData) 
      (context.activeCrossLang.id) 
      (context.activeCrossLang.code)
      (context.activeCrossLang.alphabetid)
      (context.activeCategory.id) 
      (context.dayIndex),
    
}

