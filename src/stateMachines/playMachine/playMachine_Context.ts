//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import * as O from 'fp-ts/lib/Option'

import { IPlayContext } from './playMachineTypes_Context';

import * as fp_Language from '../../Languages/fp_Language';
import * as fp_Category from '../../Categories/fp_Category';

import { gameData } from 'dblib';
import { createEmptyPuzzle, Square, createEmptyClue } from 'crosstypes';


// Warning - xState bug - may be fixed in xState v5.0
// ALL this context is trashed when the parent machine passes in ANY initial data
export const playContext: IPlayContext = {
  // Passed in
  activeCrossLang: fp_Language.createDefaultCrossLanguage(),
  activeClueLang: fp_Language.createDefaultLanguage(),
  activeCategory: fp_Category.createDefaultCategory(),
  clueWords: O.none,
  gameData: gameData,

  // Todo - this is a workaround - is there a better way to do this?
  tempPuzzleStates: {
    activePuzzle: createEmptyPuzzle(),
    activeClue:   createEmptyClue(),
    activeSquare: Square.createEmptySquare(),
  },

  activePuzzle: createEmptyPuzzle(),
  activeClue:   createEmptyClue(),
  activeSquare: Square.createEmptySquare(),

  titleLetters: 'WORDCROSSWORD',
  titleAnimationCss: '',
  points: 0,
}
