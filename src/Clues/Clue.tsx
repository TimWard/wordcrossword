//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';

import { cssJoin } from '../Utilities/cssUtils';

interface IClueProps {
  move: number
  clueNo: number
  isActive: boolean
  displayText: string
  onClueClick: () => void
  isCorrect: boolean
}

const getClassName = (props: IClueProps) =>
  cssJoin (['clueButton', 
              props.isActive ? 'isActive ' : '', 
              props.isCorrect ? 'isCorrect ' : ''])



export const Clue = React.memo (
(props: IClueProps) => 

  <li key = {props.move}>
    <button className = {getClassName (props)}
      onClick = {props.onClueClick}
    >
      <b> {props.clueNo} </b> 
      {props.displayText}
    </button>
  </li>
  
)



