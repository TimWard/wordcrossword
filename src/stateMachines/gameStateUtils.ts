//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts
import { pipe } from "fp-ts/lib/function";

// Game machine
import { TGridStates, TClueWordsStates, TPlayGameMachineStates, TGameStates } from './gameMachine/gameMachineTypes_States';
import { TPlayMachineStates } from './playMachine/playMachineTypes_States';
import { isInState } from "./crosswordStateUtils";
import { TGameState } from "./gameMachine/gameMachine_Options";
import { TPlayState } from "./playMachine/playMachine_Options";

const grids_loaded_state_hierarchy = [
  TGridStates.loadGrids, 
  TGridStates.grids_loaded,
]

const clues_loaded_state_hierarchy = grids_loaded_state_hierarchy.concat ([
  TGameStates.selectClueLang, 
  TGameStates.loadClueWords, 
  TClueWordsStates.cluewords_loaded,
])

const playgame_machine_state_hierarchy = clues_loaded_state_hierarchy.concat ([
  TPlayGameMachineStates.invoke_play_machine, 
])

export const haveCluesLoaded = (gameState: TGameState): boolean =>  
pipe (
  clues_loaded_state_hierarchy,
  isInState (gameState)
)



export const hasGameMachineLoaded = (gameState: TGameState): boolean =>  
pipe (
  playgame_machine_state_hierarchy,
  isInState (gameState)
)

export const hasGridLoaded = (gameState: TGameState): boolean =>  
  pipe (
    grids_loaded_state_hierarchy,
    isInState (gameState)
  )

export const hasPlayMachineLoaded = (playState: TPlayState): boolean =>  
pipe (
  [TPlayMachineStates.play_wrapper],
  isInState (playState)
)

  
  
