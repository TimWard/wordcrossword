//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
import { IFeedbackData } from 'dblib';

// fp-ts imports
import * as T from 'fp-ts/lib/Task'
import { pipe } from 'fp-ts/lib/pipeable';

// TSX imports
import { WebsitetextElement } from '../Websitetext/Websitetext';

// These constants should be in the websitetext table and translated
const THANKYOU_TEXT = 'Thank you for your feedback'
const PAGE_HEADER   = 'Feedback Page'
const FEEDBACK_TEXT = 'Please supply us with feedback so we can improve the game'
const SUBMIT_TEXT   = 'Submit'


interface IFeedbackProps {
  feedbackData: IFeedbackData
  getTranslatedText: (englishText: string) => string
}

export const Feedback = (props: IFeedbackProps) => 
{
  const [text, setText] = React.useState ('')

  const handleSubmit = (text: string) =>
    pipe (
      props.feedbackData.saveFeedbackToDB ({text}),
      T.map (result => setText (props.getTranslatedText (THANKYOU_TEXT)))
    )
  
  const submitDisabled = () => 
    text === '' || text === props.getTranslatedText (THANKYOU_TEXT)

  return <div>
    <h1>{props.getTranslatedText (PAGE_HEADER)}</h1>
    <p>{props.getTranslatedText (FEEDBACK_TEXT)}</p>
    <textarea className = 'feedbackTextArea'
      name='feedbackTextArea'
      onChange={(e) => setText(e.target.value)}
      value={text}
      maxLength={500}
    />
    <div>
      <button 
        className = {'button roundedbutton categoryButton ' + (submitDisabled() ? 'gray' : 'green')}
        onClick = {handleSubmit (text)}
//        onClick = {props.feedbackData.saveFeedbackToDB ({text})}
        disabled  = {submitDisabled()}
      >
        <WebsitetextElement 
          getTranslatedText = {props.getTranslatedText} 
          text              = {SUBMIT_TEXT}
        />
      </button> 
    </div>
  </div> 
}