//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------
// This file is dirty - it uses window!
//------------------------------------------------

import { ALPHABETS } from "crosstypes";
import { pipe } from "fp-ts/lib/function";

export const getQueryVariable =
(variable: string):
number =>
{
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if(pair[0] === variable) {
      return  parseInt (pair[1]);
    }
  }
  return(0);
}

export const getDayIndexFromURL = ()
: number => 
  pipe (
    getQueryVariable ('dayindex'),
    result => result > 0
      ? result
      : 1
  )



export const isAlphaChar = 
(key: string) =>
(alphabetId: number) 
: boolean => 

  // The "\b" prevents keys such as "Alt" from being entered
  alphabetId === ALPHABETS.ROMAN
    ? ( /\b[a-zA-Z]\b/.test (key) )
    // Allow only cyrillic chars for some languages
    : alphabetId === ALPHABETS.CYRILLIC
    ? ( /[а-яА-Я]/.test (key) )
    : false

export const isNumericChar = 
(key: string)
: boolean =>
  /\b[0-9]\b/.test (key)


