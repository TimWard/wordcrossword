//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import { ALPHABETS } from "crosstypes";

//---------------------------------------------
// LanguageConsts.ts
//---------------------------------------------

// We only use these language ids for testing
// The structure and data should be the same as in the DB
export const LANGUAGES = {
	TESTLANG: 	{ id:-4, 	alphabetId: ALPHABETS.ROMAN, 		name: 'Test'},
	ENGLISH: 		{ id:4, 	alphabetId: ALPHABETS.ROMAN, 		name: 'English'},
	FRENCH: 		{ id:84, 	alphabetId: ALPHABETS.ROMAN, 		name: 'French'},
	MONGOLIAN: 	{ id:54, 	alphabetId: ALPHABETS.CYRILLIC, name: 'Mongolian'},
	GERMAN: 		{ id:55, 	alphabetId: ALPHABETS.ROMAN, 		name: 'German'}
};


