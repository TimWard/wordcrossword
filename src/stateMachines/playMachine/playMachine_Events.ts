//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import { IClue, ISquare } from "crosstypes"

import * as playEvents from './playMachineTypes_Events';
import * as languageEvents from '../languageTypes_Events';

// PlayGame events
export type TSelectClueEvent = 
{
  type: typeof playEvents.SELECT_CLUE
  value: IClue
}

export type TSelectSquareEvent = 
{
  type: typeof playEvents.SELECT_SQUARE
  value: ISquare
}

export type TMyEvent = 
{
  keyCode: number, 
  key: string, 
  preventDefault: () => void,
  shiftKey: boolean,
}

export type TEnterCharEvent = 
{
  type: typeof playEvents.ENTER_CHAR
  value: TMyEvent
}

export type TRevealThis = 
{
  type: typeof playEvents.REVEAL_THIS
  value: any
}
export type TSkipThis = 
{
  type: typeof playEvents.SKIP_THIS
  value: any
}
export type TRevealAll = 
{
  type: typeof playEvents.REVEAL_ALL
  value: any
}




export type TPlayEventTypes = 
  TSelectClueEvent |
  TSelectSquareEvent |
  TEnterCharEvent |
  TRevealThis | 
  TSkipThis |
  TRevealAll  
