//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts
import { pipe } from "fp-ts/lib/function";

// xstate
import { State } from 'xstate'

// Crossword machine
import { TCrosswordStates, TPageStates, TWebsitetextStates, TCategoryStates, TPuzzleStates} from './crosswordMachine/crosswordTypes_States';
import { TCrosswordState } from "./crosswordMachine/crosswordTypes";


export const isInState = 
(machineState: State <any, any, any>) => 
(states: string[])
: boolean =>  
  machineState && 
  pipe (
    states.join('.'),
    machineState.matches
  )



  
export const hasPuzzleLoaded = (machineState: TCrosswordState): boolean =>  
  pipe (
    [
      TCrosswordStates.selectOptions, 
      TCrosswordStates.selectCrossLang, 
      TCategoryStates.selectCategory, 
      TCategoryStates.loadPuzzles, 
      TPuzzleStates.puzzles_loaded,
    ],
    isInState (machineState)
  )


export const hasWebsitetextLoaded = (machineState: TCrosswordState): boolean =>  
  pipe (
    [
      TCrosswordStates.selectOptions, 
      TCrosswordStates.selectSiteLang, 
      TCrosswordStates.loadWebsitetext, 
      TWebsitetextStates.websitetext_loaded,
    ],
    isInState (machineState)
  )


export const isOnDictionaryPage = (machineState: TCrosswordState): boolean =>  
  pipe (
    [
      TCrosswordStates.selectOptions, 
      TCrosswordStates.selectPage, 
      TPageStates.dictionary,
    ],
    isInState (machineState)
  )

  export const isOnFeedbackPage = (machineState: TCrosswordState): boolean =>  
  pipe (
    [
      TCrosswordStates.selectOptions, 
      TCrosswordStates.selectPage, 
      TPageStates.feedback,
    ],
    isInState (machineState)
  )


