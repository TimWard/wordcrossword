//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
//import * as cssUtils from '../Utilities/cssUtils';

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';

// TSX imports
import {ReactFlags} from '../Flags/flags';
import {WebsitetextElement} from '../Websitetext/Websitetext';

// fp imports
import * as fp_Languages from './fp_Languages';

// interfaces
import { ILanguage } from 'crosstypes';
import * as languageEvents from '../stateMachines/languageTypes_Events';

// Interface to the state machine
import { ISendCrosswordMachineState } from '../Game/GameStateInterface';


export interface ILanguageProps {
  sendMachineState: ISendCrosswordMachineState
  getTranslatedText: (englishText: string) => string
  title: string
  defaultLangFlag: string
  eventName: languageEvents.TLanguageEventTypes
  languages: ILanguage[]
}

export const LanguageDrowdown = React.memo (
(props: ILanguageProps) => {
  
  const onSelect = (flag: string) => 
  pipe (
    fp_Languages.findFromFlag (flag) (props.languages),
    language => props.sendMachineState ({type: props.eventName, value: language})
  )


  return (
      <div className = 'languageDropdown'>
          {/*<div style = {{color:  cssUtils.getRandomColor ()}}>Test</div>*/}
        <div className='languageDropdownTitle'>
          <WebsitetextElement 
            getTranslatedText = {props.getTranslatedText}

            text = {props.title}
          />
        </div>
        <ReactFlags 
          searchable        = {true} 
          searchPlaceholder = {props.getTranslatedText ("Select language")} 
          placeholder       = {props.getTranslatedText ("Select language")} 
          customLabels      = {fp_Languages.asPairs (props.languages)} 
          countries         = {fp_Languages.asCodes (props.languages)} 
          defaultCountry    = {props.defaultLangFlag}
          onSelect          = {onSelect}
        />
      </div>
  )

}
)
