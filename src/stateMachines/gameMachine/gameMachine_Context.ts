//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// fp-ts imports
import * as O from 'fp-ts/lib/Option'

import { IGameContext } from './gameMachineTypes_Context';

import * as fp_Language from '../../Languages/fp_Language';
import * as fp_Category from '../../Categories/fp_Category';

import { gameData } from 'dblib';
import { createEmptyPuzzle } from 'crosstypes';

// WARNING!!!
// All this context will be trashed, 
// because when this machine is invoke, and data passed in to the machine 
// completely trashes the context specified here
// This will be fixed in xState v5, apparently
export const gameContext: IGameContext = {
  // Passed in
  activeCrossLang: fp_Language.createDefaultCrossLanguage(),
  activeClueLang: fp_Language.createDefaultLanguage(),
  activeCategory: fp_Category.createDefaultCategory(),
  clueWords: O.none,  
  gameData: gameData,

  activePuzzle: createEmptyPuzzle(),
}
