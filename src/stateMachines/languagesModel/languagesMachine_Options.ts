//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { State, EventObject, ServiceConfig} from 'xstate'
import { MachineOptions, AnyEventObject, ActionFunctionMap, StateMachine } from 'xstate'
import { assign, createMachine, DoneInvokeEvent } from 'xstate'


// My functions
import * as languagesMachineDB from './languagesMachineDB';
import { ILanguage } from 'crosstypes'
import { loadingMachineConfig, ILoadingContext } from './languagesMachine';

import { TLanguageActions, TLanguageServices } from './languageTypes'


// This will all get overwritten by the context passed in by the parent machine
// This is a bug that should be fixed in xState v5
// const loadingContext: ILoadingContext = {
//   siteLangId: 4,
//   dayIndex: 1,
//   crossLangs: [],
//   clueLangs: [],
//   siteLangs: [],
// }



const langLoadingActions: ActionFunctionMap <ILoadingContext, any> = 
{
  [TLanguageActions.assignCrossLangs]: assign <ILoadingContext, DoneInvokeEvent <ILanguage[]>> 
    ({ crossLangs: (context, event) => event.data }),

  [TLanguageActions.assignClueLangs]: assign <ILoadingContext, DoneInvokeEvent <ILanguage[]>> 
    ({ clueLangs: (context, event) => event.data }),

  [TLanguageActions.assignSiteLangs]: assign <ILoadingContext, DoneInvokeEvent <ILanguage[]>> 
    ({ siteLangs: (context, event) => event.data }),

  // // Todo - replace DoneInvokeEvent with ErrorExecutionEvent?
  // [TLanguageActions.assignLangsError]: assign <ILoadingContext, DoneInvokeEvent <string>>
  //   ({ languages: (context, event) => [fp_Language.createErrorCrossLanguage (event.data)] }),
}

const langLoadingServices: Record <string, ServiceConfig <ILoadingContext>> = 
{
  [TLanguageServices.crosslang_loading]: (context, _) => 
    languagesMachineDB.loadCrossLangs (context.languageData) (context.siteLangId) (context.dayIndex),

  [TLanguageServices.cluelang_loading]: (context, _) => 
    languagesMachineDB.loadClueOrSiteLangs (context.languageData) (context.siteLangId),

  [TLanguageServices.sitelang_loading]: (context, _) => 
    languagesMachineDB.loadClueOrSiteLangs (context.languageData) (context.siteLangId),
}

const loadingLangMachineOptions: Partial<MachineOptions<ILoadingContext, AnyEventObject>> =
{
  services: langLoadingServices,
  actions: langLoadingActions,
}

export type ILoadingMachine = StateMachine <ILoadingContext, any, EventObject>
export type TLoadingState = State <ILoadingContext, EventObject, any>

export const langLoadingMachine: ILoadingMachine = 
  createMachine <ILoadingContext, any, TLoadingState> 
    (loadingMachineConfig, loadingLangMachineOptions)
//      .withContext (loadingContext)

