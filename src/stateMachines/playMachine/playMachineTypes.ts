//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as playMachineEvents from './playMachine_Events';
import { TPlayState } from './playMachine_Options';

type IPlayEventType = 
string 
| playMachineEvents.TPlayEventTypes

export type ISendPlayState = (event: IPlayEventType, payload?: any) => TPlayState

