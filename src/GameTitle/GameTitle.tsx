//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
//import * as cssUtils from '../Utilities/cssUtils';

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';
import * as A from 'fp-ts/lib/Array'

// JSX imports
import {GameTitleLetter} from '../GameTitle/GameTitleLetter';

// Interface to the state machine
import * as playEvents from '../stateMachines/playMachine/playMachineTypes_Events';
import { ISendPlayState } from '../stateMachines/playMachine/playMachineTypes';
import { splitTitleLetters } from './fp_GameTitle';


interface IGameTitleProps {
  titleLetters: string,
  titleAnimationCss: string,
	sendPlayState: ISendPlayState
}

// The title letters will be spread out over 1 to 3 rows, depending on the screen size!
// Note that React won't allow you to return an array of JSX.Element, it has to be a single element
export const GameTitle = 
React.memo ( 
(props: IGameTitleProps)
: JSX.Element => {    

  return <div className="titlerowcenter">
      {/*<div style = {{color:  cssUtils.getRandomColor ()}}>Test</div>*/}
    {pipe (
        splitTitleLetters (props.titleLetters),
        A.map (renderTitleRow (props) (props.titleAnimationCss)), 
    )}
  </div>
}
)



const renderTitleRow = 
(props: IGameTitleProps) =>
(animationCss: string) =>
(titleRow: string)
: JSX.Element => 
  <div className="titlerow">
    {splitTitleRow (props) (titleRow) (animationCss)}
  </div>

const splitTitleRow = 
(props: IGameTitleProps) =>
(titleRow: string) =>
(animationCss: string)
: JSX.Element[] =>

  pipe (
    titleRow.split (''),
    A.map (renderTitleLetter (props) (animationCss))
  )


const renderTitleLetter = 
(props: IGameTitleProps) =>
(animationCss: string) =>
(letter: string) 
: JSX.Element => 
  <GameTitleLetter 
    letter = {letter}
    onClick = {() => props.sendPlayState ({type: playEvents.ENTER_CHAR, 
      value: 
        {
          keyCode: -1, 
          key: letter, 
          preventDefault: () => undefined,
          shiftKey: false,
        }
      })
    }    
    animationCss = {animationCss}
  />

