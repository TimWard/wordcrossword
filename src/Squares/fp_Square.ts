//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

/*eslint @typescript-eslint/no-unused-expressions: 0*/


//------------------------------------------------
// This file is pure - no tasks, no IO
//------------------------------------------------

import {Draft} from 'immer';

// Pure imports
import { ISquare, ISquareCallback } from "crosstypes";
import { IClue } from "crosstypes";


// fp imports
import { isUndefined } from "util";





  

export const doSquaresShareClue =
  (square1: ISquare) => (square2: ISquare)
  : boolean => 

  (!isUndefined (square1.clueAcross) && !isUndefined (square2.clueAcross) && (square1.clueAcross === square2.clueAcross) ) 
  ||
  ( !isUndefined (square1.clueDown) && !isUndefined (square2.clueDown) && (square1.clueDown === square2.clueDown) )

export const hasSquareLetter = 
  (square: ISquare)
  : boolean =>
  square.userLetter !== ''


export const getPriorityClue = 
  (square: ISquare)
  : IClue => 
    square.clueAcross 
      ? square.clueAcross 
      : square.clueDown
        ? square.clueDown
        : {} as IClue // Shouldn't reach here

export const getPerpendicularClue = 
  (square: ISquare) =>
  (clue: IClue):
  IClue =>
      square.clueAcross && (square.clueAcross === clue)
      ? (square.clueDown) ? square.clueDown : clue
      : (square.clueAcross) ? square.clueAcross : clue
  
export const doesSquareMatchClue =
  (square: ISquare) =>
  (clue: IClue):
  boolean => 
  clue === square.clueAcross || 
  clue === square.clueDown
    
// The following functions are called by button event handlers
export const mutate_reveal: ISquareCallback =
(square: Draft <ISquare>)
: void => {
  square.userLetter = square.answer
  return undefined
}



