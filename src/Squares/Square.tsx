//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
//import * as cssUtils from '../Utilities/cssUtils';

// Pure interfaces
import { ISquare } from 'crosstypes';
import { Square as SquareClass } from "crosstypes";
import { CATEGORIES } from 'crosstypes';

// Interface to the state machine
import * as playEvents from '../stateMachines/playMachine/playMachineTypes_Events';
import { ISendPlayState } from '../stateMachines/playMachine/playMachineTypes';
import { cssJoin } from '../Utilities/cssUtils';

import * as fp_Square from '../Squares/fp_Square';

// CSS consts
const CATEGORIES_CSS        = ['large', 'medium', 'small']
const BLINKING_CURSOR_CSS   = 'blinking-cursor'
const SQUARE_CSS            = 'square'
const SQUARE_LETTER_CSS     = 'square_letter'
const SQUARE_CLUENO_CSS     = 'square_clueNo'

export interface IGridPropsForSquareProps {
	readonly sendPlayState: ISendPlayState
  readonly activeCategoryId: number
}

 export interface ISquareProps extends IGridPropsForSquareProps{
  readonly square: ISquare
  readonly isActive: boolean
  readonly isCrosswordCorrect: boolean
  readonly isClueActive: boolean
}

// The squares are coloured in the following order
const SQUARECOLOURS = {
  isBlack:               "isBlack",
  isCrosswordCorrect:    "isCrosswordCorrect",
  isCorrect:             "isCorrect",
  isInCorrect:           "isInCorrect",
  isClueActive:          "isClueActive",
  isEmpty:               "isEmpty",
}

const calculateSquareBackgroudColourCssClassName = (props: ISquareProps)
: string => 
  (props.square.isBlack) ? SQUARECOLOURS.isBlack :
    props.isCrosswordCorrect ? SQUARECOLOURS.isCrosswordCorrect :
      SquareClass.isSquareCorrect (props.square) ? SQUARECOLOURS.isCorrect :
        fp_Square.hasSquareLetter (props.square) ? SQUARECOLOURS.isInCorrect :
          props.isClueActive ? SQUARECOLOURS.isClueActive : 
            SQUARECOLOURS.isEmpty

export const Square = 
//React.memo (
(props: ISquareProps) => {
  var squareRef: HTMLDivElement | null

  const focusSquare = () => 
    props.isActive && squareRef ? squareRef.focus() : undefined

  React.useEffect (focusSquare)

  const onSquareClick = (square: ISquare) => 
    square.isBlack 
      ? undefined 
      : props.sendPlayState ({type: playEvents.SELECT_SQUARE, value: square})


  return (
    <div 
      ref       = {ref => squareRef = ref} 
      id 				= {'Square_' + props.square.row + '_' + props.square.col}
      className = {cssJoinPlusCategory ([SQUARE_CSS, calculateSquareBackgroudColourCssClassName (props)], props.activeCategoryId)}
//      style 		= {{backgroundColor: 	cssUtils.getRandomColor (),
      onKeyDown	=	{(event: React.KeyboardEvent<HTMLDivElement>) => {
        props.sendPlayState ({type: playEvents.ENTER_CHAR, 
          value:  
          { keyCode: event.keyCode,
            key: event.key, 
            preventDefault: event.preventDefault, 
            shiftKey: event.shiftKey
          }, 
        })
      }
      }
      onClick	=	{(myEvent: any) => onSquareClick (props.square)}
      tabIndex 	= {0}
    >
      <div className = {cssJoinPlusCategory ([SQUARE_CLUENO_CSS], props.activeCategoryId)}>
        {props.square.clueNo}
      </div>
      <div className = {cssJoinPlusCategory ([SQUARE_LETTER_CSS, getLetterCssClassNameByActive (props)], props.activeCategoryId)}>
          {getSquareLetter (props)}
      </div>
    </div>
  )
}
//)


//-------------------------------------------------------------------------------
// fp function private to this unit
//-------------------------------------------------------------------------------

const CURSORCHAR = '?';
const getSquareLetter =
(squareProps: ISquareProps)
: string => 
  false
  ? squareProps.square.userLetter 
    ? squareProps.square.userLetter.toUpperCase() 
    : squareProps.isActive  
      ? CURSORCHAR 
      : squareProps.square.answer 
        ? squareProps.square.answer.toUpperCase() 
        : squareProps.square.answer
  : squareProps.square.userLetter 
    ? squareProps.square.userLetter.toUpperCase() 
    : squareProps.isActive ? CURSORCHAR : ''

  
//--------------------------------------------------------------------
// CSS functions
//--------------------------------------------------------------------

const cssJoinPlusCategory = 
(arr: string[], categoryId: number): string => 
  cssJoin (arr.concat (getCssClassNameForCategory (categoryId)))





const getCssClassNameForCategory =
(categoryId: number)
: string => 

  categoryId === CATEGORIES.BEGINNER
    ? CATEGORIES_CSS[0]
    : categoryId === CATEGORIES.INTERMEDIATE
      ? CATEGORIES_CSS[1] 
      : CATEGORIES_CSS[2] 


const getLetterCssClassNameByActive =
(squareProps: ISquareProps)
: string => 

  squareProps.isActive 
    ? BLINKING_CURSOR_CSS
    : ''

//--------------------------------------------------------------------
// End of - CSS functions
//--------------------------------------------------------------------
