//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------


// xstate
import { ActionFunctionMap } from 'xstate'
import { assign } from 'xstate'
import { send } from 'xstate';


import { IPlayContext } from './playMachineTypes_Context';

import * as playEvents from './playMachineTypes_Events';
import * as playMachineActions from './playMachineTypes_Actions';
import * as playMachineEvents from './playMachine_Events';
import * as eventHandlers from '../../Game/fp_gameEventHandlers_xstate';

import * as fp_gamePure from '../../Game/fp_gamePure';

import { ASCII } from '../../Consts/AsciiConsts';

          

export const playActions: ActionFunctionMap <IPlayContext, any> = 
{
  // Title Letters
  [playMachineActions.TTitleLettersActions.assignTitleLetters]: assign <IPlayContext, any>
    ({ titleLetters: (context, event) =>  context.activeClue.titleLetters }),

    
  [playMachineActions.TTitleLettersActions.assignTitleAnimationCss1]: assign <IPlayContext, any>
    ({ titleAnimationCss: (context, event) =>  'titleletteranimation1' }),

  [playMachineActions.TTitleLettersActions.assignTitleAnimationCss2]: assign <IPlayContext, any>
    ({ titleAnimationCss: (context, event) =>  'titleletteranimation2' }),


  [playMachineActions.TPlayActions.addPoints]: assign <IPlayContext, any> 
    ({ points: (context, event) => context.points + 
      Math.ceil (context.activeClue.crossWord.id / 500) * 10  }),

  [playMachineActions.TPlayActions.moveToNextClue]: assign <IPlayContext, any> 
    ({ 
      tempPuzzleStates: (context, event) => 
        fp_gamePure.getActiveClueAndActiveSquare (context) (context.activeClue)
    }),

    [playMachineActions.TPlayActions.moveToNextSquare]: assign <IPlayContext, any> 
    ({ 
      tempPuzzleStates: (context, event) => 
        fp_gamePure.getNextActiveSquare (context) (context.activeClue)
    }),

    
  [playMachineActions.TPlayActions.assignActiveClue]: 
    assign <IPlayContext, playMachineEvents.TSelectClueEvent> 
  ({
    tempPuzzleStates: (context, event) => 
      eventHandlers.onSelectClue (context) (event.value),
  }),
            
 
  [playMachineActions.TPlayActions.squareKeyDown]: 
    send ((context: IPlayContext, event: playMachineEvents.TEnterCharEvent) => 
  
    event.value.keyCode === ASCII.TAB
      ? ({type: playEvents.ENTER_TAB, value: event.value})          
      : [ASCII.LEFT, ASCII.RIGHT, ASCII.UP, ASCII.DOWN].includes (event.value.keyCode)
        ? ({type: playEvents.ENTER_ARROW, value: event.value})          
        : ({type: playEvents.ENTER_LETTER, value: event.value})),

                
    [playMachineActions.TPlayActions.squareUpdate]: 
      send ((context: IPlayContext, event: any) => 
        ({type: playEvents.SQUARE_UPDATE, value: event.value})),

    [playMachineActions.TPlayActions.enterLetter]: 
      assign <IPlayContext, playMachineEvents.TEnterCharEvent> 
      ({ 
        tempPuzzleStates: (context, event) => 
          eventHandlers.onLetterKey (context) (event.value),
      }),
  
    [playMachineActions.TPlayActions.enterArrow]:  
    assign <IPlayContext, playMachineEvents.TEnterCharEvent> 
    ({ 
      tempPuzzleStates: (context, event) => 
        eventHandlers.onArrowKey (context) (event.value),
    }),

    [playMachineActions.TPlayActions.enterTab]:  
    assign <IPlayContext, playMachineEvents.TEnterCharEvent> 
    ({ 
      tempPuzzleStates: (context, event) => 
        eventHandlers.onTabKey (context) (event.value),
    }),

  
    [playMachineActions.TPlayActions.revealThis]:  
    assign <IPlayContext, playMachineEvents.TRevealThis> 
    ({ 
      tempPuzzleStates: (context, event) => 
        eventHandlers.onRevealThisClick (context),
    }),

    [playMachineActions.TPlayActions.skipThis]:  
    assign <IPlayContext, playMachineEvents.TSkipThis> 
    ({ 
      tempPuzzleStates: (context, event) => 
        eventHandlers.onSkipThisClick (context),
    }),

    [playMachineActions.TPlayActions.revealAll]:  
    assign <IPlayContext, playMachineEvents.TRevealAll> 
    ({ 
      tempPuzzleStates: (context, event) => 
        eventHandlers.onRevealAllClick (context),
    }),


  [playMachineActions.TPlayActions.assignFromActivePuzzleStates]:  
    assign <IPlayContext, playMachineEvents.TEnterCharEvent> 
    ({
      titleLetters: (context, event) => context.tempPuzzleStates.activeClue.titleLetters,

      activePuzzle: (context, event) => 
        context.tempPuzzleStates.activePuzzle,
      activeClue: (context, event) => 
          context.tempPuzzleStates.activeClue,
      activeSquare: (context, event) => 
        context.tempPuzzleStates.activeSquare,
    }),

  [playMachineActions.TPlayActions.assignInitialActiveClue]: assign <IPlayContext, any> 
    ({ 
      activeClue: (context, _) => context.activePuzzle.clues[0],
      activeSquare: (context, _) => fp_gamePure.getActiveSquareFromActiveClue (context.activePuzzle.grid.squares) (context.activePuzzle.clues[0]) 
    }),

}

