//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------
// This file is pure - no tasks, no IO
//------------------------------------------------

// Other 3rd party imports
import produce, {Draft} from 'immer';

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';
import * as A from 'fp-ts/lib/Array'
import * as O from 'fp-ts/lib/Option'
import * as Ord from 'fp-ts/lib/Ord'


// class imports
import { IClue, Clue } from "crosstypes";
import { ISquare } from "crosstypes";
import { IBlankword } from 'crosstypes';

// fp imports
import {cf_Blankword, cf_Squares} from 'crossfuncs';
import * as fp_GameTitleLetter from '../GameTitle/fp_GameTitleLetter';
import * as fp_GameTitle from '../GameTitle/fp_GameTitle';
import { Word } from 'crosstypes';



export const getClue =
(clues: IClue[]) =>
(clueId: number):
IClue => 
  clues[clueId]


// A.rotate rotates right and we want rotate left plus one,
// so that the active clue is at the end of the list
const rotateLeftPlusOne = 
(n: number) => 
<A>(as: Array<A>)
: Array<A> =>
  A.rotate (as.length - n - 1) (as)

  

// Returns the next (or prev) incorrect clue 
// or the current clue if the puzzle has been solved
export const getNextIncorrectClueByDirection = 
(forward: boolean) => 
(squares: ISquare[][]) =>
(clues: IClue[]) =>
(currentClue: IClue)
: IClue =>

  pipe (
    forward ? clues : A.reverse <IClue> (clues),
    directedClues => rotateLeftPlusOne (directedClues.indexOf (currentClue)) (directedClues),
    A.filter <IClue> (clue => !cf_Blankword.isCorrect (squares) (clue.blankword)),
    A.head,
    O.fold(
      () => currentClue,
      nextClue => nextClue
    )
  )
 
export const getNextIncorrectClue = getNextIncorrectClueByDirection (true)

// Some clues share the same clue no
// In this case, if the user presses the same clue no twice
// then we need to alternate between the clues
export const findClueByClueNo =
(squares: ISquare[][]) =>
(clues: IClue[]) =>
(clueNo: number) =>
(activeClue: IClue)
: IClue => 

  pipe (
    clues.filter(clue => Clue.clueNo (clue) === clueNo && !cf_Blankword.isCorrect (squares) (clue.blankword)),
    result => result.length === 0
      ? activeClue
      : result.length === 2 && result[0] === activeClue
        ? result[1]
        : result[0]
  )
  


export const sortByDirectionByClueNo =
(clues: IClue[])
: IClue[] => {
  const byDirection = Ord.ord.contramap(Ord.ordNumber, (c: IClue) => Clue.direction (c))
  const byClueNo = Ord.ord.contramap(Ord.ordNumber, (c: IClue) => Clue.clueNo (c))

  return pipe (
    clues,
    A.sortBy([byDirection, byClueNo]) 
  )
}  




export const assignBlankwordToClue =
(blankwords: IBlankword[]) =>
(clue: IClue) 
: IClue => 
  pipe (
    blankwords,
    A.findFirst <IBlankword> (blankword =>  blankword.id === clue.blankwordid),
    O.fold(
      () => clue,
      blankword => produce (clue, draft => {draft.blankword = blankword})      
    ),
  )
      
 
export const updateSquaresForAllClues =
(squares: Draft <ISquare> [][]) =>
(clues: IClue[]) :
void[][] => 
  clues.map (cf_Squares.updateSquaresForClue (squares))

    
  
export const createRandomPaddedTitle = 
(alphabetId: number) => 
(languageCode: string) => 
(clue: IClue)
: IClue => 

  produce (clue, draft => {
    draft.titleLetters = pipe (
      Word.getCrossText (clue.crossWord),
      crossword => fp_GameTitleLetter.padWordWithRandomLetters 
        (crossword) 
        (fp_GameTitle.getTitleLengthByScreenSize()) 
        (fp_GameTitleLetter.getAlphabet (alphabetId))
        (languageCode)
    )
  })      

