//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';


interface IGameTitleLetterProps {
  letter: string
  onClick: (letter: string) => void
  animationCss: string
}


export const GameTitleLetter =
(props: IGameTitleLetterProps) => {

  return <div className = 'titlesquare blue' onClick = {(letter) => props.onClick (props.letter)}>
    <div className = {'titleletter ' + props.animationCss}> 
      {props.letter}
    </div>
  </div>
}



