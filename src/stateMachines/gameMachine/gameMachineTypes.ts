//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as gameMachineEvents from '../gameMachine/gameMachine_Events';
import { TGameState } from './gameMachine_Options';

type IGameEventType = 
string 
| gameMachineEvents.TGameEventTypes

export type ISendGameState = (event: IGameEventType, payload?: any) => TGameState

