//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// --------------------------------------------------------------------------
// Note that this file is used by the game machine and the crossword machine!
// --------------------------------------------------------------------------

// ------------------------------------------
// Languages
// ------------------------------------------
export const SELECT_CROSSLANG     = 'SELECT_CROSSLANG'
export const SELECT_SITELANG      = 'SELECT_SITELANG'
export const SELECT_CLUELANG      = 'SELECT_CLUELANG'

export const TLanguageEvents = {
  SELECT_CROSSLANG: SELECT_CROSSLANG,
  SELECT_SITELANG: SELECT_SITELANG,
  SELECT_CLUELANG: SELECT_CLUELANG,
} 

export type TLanguageEventTypes = 
  typeof SELECT_CROSSLANG |
  typeof SELECT_SITELANG |
  typeof SELECT_CLUELANG
 
