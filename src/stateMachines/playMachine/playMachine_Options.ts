//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

// xstate
import { State, EventObject,MachineOptions, AnyEventObject, StateMachine, createMachine } from 'xstate'


// My functions
import { playMachineConfig } from './playMachine';

import { IPlayContext } from './playMachineTypes_Context';

import { playContext } from './playMachine_Context';
import { playActions } from './playMachine_Actions';
import { playGuards } from './playMachine_Guards';


const playMachineOptions: Partial<MachineOptions<IPlayContext, AnyEventObject>> =
{
  actions: playActions,
  guards: playGuards,
}


export type IPlayMachine = StateMachine <IPlayContext, any, EventObject>
export type TPlayState = State <IPlayContext, EventObject, any>

export const playMachine: IPlayMachine = 
  createMachine <IPlayContext, any, TPlayState> 
    (playMachineConfig, playMachineOptions)
      .withContext (playContext)


