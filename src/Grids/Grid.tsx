//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
//import * as cssUtils from '../Utilities/cssUtils';

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';
import * as A from 'fp-ts/lib/Array'
import { identity } from 'fp-ts/lib/function';

// JSX imports
import {Square, IGridPropsForSquareProps} from '../Squares/Square';

// fp imports
import { IPuzzle, IClue, ISquare } from 'crosstypes';

import * as fp_Squares from '../Squares/fp_Squares';
import {cf_Blankword} from 'crossfuncs';



interface IGridProps extends IGridPropsForSquareProps {
  readonly activeClue: IClue
  readonly activePuzzle: IPuzzle
  readonly activeSquare: ISquare
  readonly screenWidth: number
}


      

// Todo - reintroduct this line
export const Grid = 
//React.memo (
//export const Grid =
(props: IGridProps) => {


  const renderSquare = (row: number) => (col: number) => 

    pipe (
      props.activePuzzle.grid.squares,
      squares => pipe (
        squares[row][col],
          currSquare => 
        <Square 
          square                    = {currSquare}
          sendPlayState             = {props.sendPlayState}
          isActive                  = {!fp_Squares.isCrosswordCorrect (squares) && (props.activeSquare === currSquare)}
          activeCategoryId          = {props.activeCategoryId}
          isCrosswordCorrect        = {fp_Squares.isCrosswordCorrect (squares)}
          isClueActive              = {cf_Blankword.containsSquare (squares) (props.activeClue.blankword) (currSquare)}
        />
      )
    )

  const renderRow = (row: number) => 
    <div className="board-row" key={row}>
      {
        pipe (
          A.makeBy (props.activePuzzle.grid.boardsize, identity),
          A.map (renderSquare (row))
        )
      }
    </div>


//<div style = {{color:  cssUtils.getRandomColor ()}}>Active Cat: {props.activeCategoryId}</div>
  const renderGrid = () => 
    
    <div className="grid" key={0}>      
      { pipe (
          A.makeBy (props.activePuzzle.grid.boardsize, identity),
          A.map (renderRow) 
      ) }
    </div>
  

  return renderGrid ()

}
//)

