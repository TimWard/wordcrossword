//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------
// This file is almost pure - no tasks, no IO
// - but it does use random!
//------------------------------------------------

// fp-ts imports
import * as S from "fp-ts/lib/Set";
import { ordString } from "fp-ts/lib/Ord";

// Interfaces / consts
import { ALPHABETS } from "crosstypes";

// Other 3rd party imports
const memoize = require('memoizee');


export const _getAlphabet = 
  (alphabetId: number)
  : string[] => 
  
  (alphabetId === ALPHABETS.ROMAN) 
    ? Array( 26 ).fill( 1 ).map( ( _, i ) => String.fromCharCode( 65 + i ) )
    : (alphabetId === ALPHABETS.CYRILLIC) 
      ? Array( 0x1F ).fill( 1 ).map( ( _, i ) => String.fromCharCode( 0x410 + i ) )
      : [] 

export const getAlphabet = memoize (_getAlphabet)


const getRandomLetter = 
  (alphabet: string[])
  : string =>
    alphabet[Math.floor(Math.random() * alphabet.length)]


export const padWordWithRandomLetters =
  (word: string) => 
  (length: number) =>
  (alphabet: string[]) =>
  (locale: string)
  : string => {
  
  
  var mySet = new Set (word)

  while (mySet.size < length) 
    mySet.add (getRandomLetter (alphabet))

  
  // This sorts the letters in alphabetically order and converts to an array
  // Can't use fp-ts because this uses localeCompare!
  const mynewSet = S.toArray  (ordString) (mySet)
  const result = mynewSet.sort ( (a: string, b: string) => a.localeCompare(b, locale))
  return result.reduce ( (a,b) => a.concat (b))
}


