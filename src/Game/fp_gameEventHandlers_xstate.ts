//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------------
// This file is pure - no tasks, no IO, no dirty imports
//------------------------------------------------------

// fp-ts imports
import { pipe } from 'fp-ts/lib/pipeable';

// interfaces
import {ISquare} from 'crosstypes'
import { ASCII } from '../Consts/AsciiConsts';

// fp imports - all pure!
import * as fp_gamePure from './fp_gamePure';
import * as fp_Square from '../Squares/fp_Square';


import { TMyEvent } from '../stateMachines/playMachine/playMachine_Events';
import { IClue } from 'crosstypes';
import { IPlayContext, IPuzzleStates } from '../stateMachines/playMachine/playMachineTypes_Context';


export const onRevealAllClick = 
(states: IPuzzleStates)
: IPuzzleStates => 
  fp_gamePure.executeForAllSquares (states) (fp_Square.mutate_reveal)

export const onSkipThisClick = 
(states: IPlayContext)
: IPuzzleStates => 
  fp_gamePure.getNextIncorrectClueAndSquare (states)

export const onRevealThisClick = 
(states: IPlayContext)
: IPuzzleStates => 
  fp_gamePure.executeForAllSquaresInClue (states) (fp_Square.mutate_reveal)


export const onSelectClue = 
(states: IPlayContext) =>
(clue: IClue)
:IPuzzleStates  => 
  fp_gamePure.doSelectClue (states) (clue)


export const onSelectSquare = 
(states: IPlayContext) =>
(square: ISquare)
:IPuzzleStates  => 
  fp_gamePure.doSelectSquare (states) (square)



    

export const onLetterKey = 
(states: IPlayContext) =>
(event: TMyEvent)
:IPuzzleStates  => 
 fp_gamePure.newHandleSquareKeyPress (states) (event.key)

        
export const onArrowKey = 
(states: IPlayContext) =>
(event: TMyEvent)
:IPuzzleStates  => 
  pipe (
    event.keyCode === ASCII.RIGHT || event.keyCode === ASCII.DOWN,
    fp_gamePure.handleTabKeyPress (states) 
  )


export const onTabKey = 
(states: IPlayContext) =>
(event: TMyEvent)
:IPuzzleStates  => 

  pipe (
    event.preventDefault(),
    () => fp_gamePure.handleTabKeyPress (states) (!event.shiftKey),
  )


  


