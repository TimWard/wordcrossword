//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//-----------------------------------------------------------------
// This import is dirty - it uses window
//-----------------------------------------------------------------
import * as fp_Screen from '../Screen/fp_Screen';
import { isScreenWidth768 } from '../Screen/fp_Screen';


export const splitTitleLetters =
  (titleLetters: string)
  : string[] => 

  isScreenWidth768()
    ? [titleLetters]
    : titleLetters.length === 13
      ? [titleLetters.slice(0,4),
            titleLetters.slice(4,9),
            titleLetters.slice(9,13)]
      : [titleLetters.slice(0,4),
         titleLetters.slice(4,9)]


export const getTitleLengthByScreenSize = 
(): number =>
  fp_Screen.isScreenWidth768 () ? 13 : 13
