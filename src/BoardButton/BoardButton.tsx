//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
import {WebsitetextElement} from '../Websitetext/Websitetext';

// Interface to the state machine
import { IBoardButtonsProps } from './BoardButtons';

// siteLangId is not used directly, 
// but we want the button to rerender when the site language changes,
// because we need to reload the translated text
export interface IBoardButtonProps {
  props: IBoardButtonsProps
  eventId: string
  getText: () => string
  
}

export const BoardButton = (props: IBoardButtonProps) => {
  
  const buttonEnabled = props.props.playState
    ? props.props.playState.nextEvents.includes (props.eventId)
    : false

  const getButtonClassName = 
  (isEnabled: boolean) => 
  isEnabled
      ? "button roundedbutton boardButton blue"
      : "button roundedbutton boardButton gray "
  
  return <button 
      className = {getButtonClassName (buttonEnabled)}
      onClick   = {() => props.props.sendPlayState (props.eventId)} 
    >
    <WebsitetextElement 
      getTranslatedText = {props.props.getTranslatedText} 
      text = {props.getText ()}
    />
  </button>


}    


