//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

export interface IWebsitetextProps {
  getTranslatedText: (englishText: string) => string
  text: string
}


export interface IWebsitetextExtraProps {
  getTranslatedText: (englishText: string) => string
  text: string
  additionalText: string
}


export type IWebsitetextElement = (props: IWebsitetextProps) => JSX.Element
