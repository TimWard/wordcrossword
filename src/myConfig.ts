//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------
import {BUILD_TARGETS} from 'dblib';

export const buildConfig = {
//  target: BUILD_TARGETS.DEBUG  // log level 2
  // target: BUILD_TARGETS.TEST  // log level 3
   target: BUILD_TARGETS.LIVE // log level 4
}

