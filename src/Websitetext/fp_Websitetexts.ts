//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

//------------------------------------------------
// This file is pure - no tasks, no IO, no dirty imports
//------------------------------------------------

// fp-ts imports
import { pipe } from "fp-ts/lib/pipeable";

// pure imports
import { IWebsitetext } from "crosstypes";

  
export const createEmptyWebsitetext = (): IWebsitetext =>
{ return {
  id: -1,
  englishtext: '',
  text: '',
}}

export const getTranslatedText =
(websitetexts: IWebsitetext[]) => 
(englishText: string):
string => 
  pipe (
   websitetexts.find (record => record.englishtext === englishText),
   websitetext => websitetext
     ? websitetext.text
     :`${englishText}!`
  )

