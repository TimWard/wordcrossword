//--------------------------------------------------------
// Copyright (c) 2020 Tim Ward
//--------------------------------------------------------

import * as React from 'react';
//import * as cssUtils from '../Utilities/cssUtils';

// TSX imports
import { WebsitetextElement } from '../Websitetext/Websitetext';

// interface imports
import { State } from 'xstate';

// Interface to the state machine
import * as crosswordEvents from '../stateMachines/crosswordMachine/crosswordTypes_Events';
import { ISendCrosswordMachineState } from '../Game/GameStateInterface';
import { PAGE_CONSTS } from '../Consts/PageConsts';

interface IGamePageButtonsProps {
  sendMachineState: ISendCrosswordMachineState
  getTranslatedText: (englishText: string) => string
  machineState: State <any,any,any>
  // This property isn't used directly,
  // but we need to refresh when the site language changes
  siteLangId: number 
}

export const GamePageButtons = React.memo ( 
(props: IGamePageButtonsProps)
: JSX.Element => 

  <div className="pages">
      {/*<div style = {{color:  cssUtils.getRandomColor ()}}>Test</div>*/}
    <div className="pagesHeader">
      <WebsitetextElement 
        getTranslatedText = {props.getTranslatedText} 
        text = "Pages"
      />
    </div>
    <ol>
      {createPageButton (props, 'crosswords-button', crosswordEvents.TPageEvents.CLICK_CROSSWORDS, PAGE_CONSTS.CROSSWORDS_TEXT)}
      {/* {createPageButton (props, 'dictionary-button', crosswordEvents.TPageEvents.CLICK_DICTIONARY, PAGE_CONSTS.DICTIONARY_TEXT)} */}
      {createPageButton (props, 'feedback-button', crosswordEvents.TPageEvents.CLICK_FEEDBACK, PAGE_CONSTS.FEEDBACK_TEXT)}
    </ol>
  </div>

)


const createPageButton = (props: IGamePageButtonsProps, testId: string,
  eventId: string, buttonText: string)
  : JSX.Element => {

  const buttonEnabled = props.machineState.nextEvents.includes (eventId)

  return <button 
    data-testid={testId}
    disabled  = {!buttonEnabled}
    className = {getPageButtonCssClass (!buttonEnabled)}
    onClick   = {() => props.sendMachineState (eventId) } 
  >

    <WebsitetextElement 
      getTranslatedText = {props.getTranslatedText} 
      text              = {buttonText}
    />
  </button>
 
}
  

const getPageButtonCssClass = 
(isActive: boolean) => 
isActive
    ? "button roundedbutton green pageButton"
    : "button roundedbutton blue pageButton"
